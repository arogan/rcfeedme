﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace rcFeedMe
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            

            if (args.Length > 0 && args[0] == "4")
            {
                frmRcFeedMe f1 = new frmRcFeedMe(args);
                
                
                Application.Run();
                f1.Close();
                f1.Dispose();
                Application.Exit();
            }
            else
            {
                Application.Run(new frmRcFeedMe(args));
            }
        }
    }
}
