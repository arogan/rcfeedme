﻿/*
 * Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RcSoft.RcCommon;
using System.Xml.Serialization;

namespace rcFeedMe
{
    public class Config
    {
        public const string XMLName = "Config.xml";
        public readonly string XMLFullName = RcShared.GetExePath(false) + @"\" + Config.XMLName;

        public int Freq = 360;
        public string SavePath = @"c:\podcasts";
        public string SyncPath = "";
        public int KeepCount = 10;
        private int scanThread = 4;
        public int ScanThread
        {
            get 
            {
                if (scanThread > ScanMax)
                {
                    scanThread = ScanMax;
                }
                return scanThread; 
            }
            set { scanThread = value; }
        }
        private int dlThread = 2;

        public int DLThread
        {
            get 
            {
                if (dlThread > DLMax)
                {
                    dlThread = DLMax;
                }
                return dlThread; 
            }
            set { dlThread = value; }
        }
        public int ScanStall = 15;
        public int DLStall = 30;
        public int ScanTimeout = 300;
        public int DLTimeout = 0;        
        public int ScanRetry = 3;
        public int DLRetry = 3;
        public string TimeZones = "NST|NDT|AST|ADT|EST|EDT|CST|CDT|MST|MDT|PST|PDT|AKST|AKDT|HAST|HADT";
        public string DateFormats = "ddd MMM d HH:mm:ss K yyyy|MMM d HH:mm:ss yyyy|MMM d HH:mm:ss K yyyy";
        public bool LogDupErrors = false;

        private const int ScanMax = 16;
        private const int DLMax = 6;    
                
        public static readonly string tokenFilename = "%filename%";
        public static readonly string tokenFullname = "%fullname%";       
        public static readonly string tokenSavePath = "%savepath%";
        public static readonly string tokenGroupname = "%groupname%";
        public static readonly string tokenSyncPath = "%syncpath%";
        public static readonly string tokenFeedname = "%feedname%";
        public static readonly string tokenDate = "%date%";

    }
}
