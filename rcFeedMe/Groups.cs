﻿/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using RcSoft.RcCommon;

namespace rcFeedMe
{
    public class Groups : System.Collections.ObjectModel.KeyedCollection<string, Group>
    {
        public const string XMLName = "Groups.xml";
        public readonly string XMLFullName = RcShared.GetExePath(false) + @"\" + Groups.XMLName;
        public const string None = "(None)";

        protected override string GetKeyForItem(Group g)
        {
            return g.Name;
        }

        public void ExportDataTable(DataTable dt)
        {
            if (this.Count == 0)
            {
                Group g = new Group();
                g.Name = "Default";
                g.SavePath = @"c:\podcasts";
                this.Add(g);
            }

            dt.Clear();
            //DataTable dt = new DataTable();
            //dt.Columns.Add("Name", typeof(System.String));
            //dt.Columns.Add("SavePath", typeof(System.String));
            DataRow dr;

            //if (addNone)
            //{
            //    dr = dt.NewRow();
            //    dr["Name"] = Groups.None;
            //    dr["SavePath"] = "";
            //    dt.Rows.Add(dr);
            //}
            foreach (Group g in this)
            {
                dr = dt.NewRow();
                dr["Name"] = g.Name;
                dr["SavePath"] = g.SavePath;
                dr["SyncPath"] = g.SyncPath;
                dr["BareSyncPath"] = g.BareSyncPath;
                dr["PlaylistPath"] = g.PlaylistPath;
                dr["PreSyncExeGroup"] = g.PreSyncExeGroup;
                dr["PreSyncExeArgsGroup"] = g.PreSyncExeArgsGroup;
                dr["SuppressSyncCopy"] = g.SuppressSyncCopy;
                if (g.PlaylistCutoffDays.HasValue)
                    dr["PlaylistCutoffDays"] = g.PlaylistCutoffDays.Value;
                dt.Rows.Add(dr);
            }            

            //return dt;
        }

        private int SortByName(Group g1, Group g2)
        {
            return g1.Name.CompareTo(g2.Name);
        }

        public void Sort()
        {
            List<Group> list = base.Items as List<Group>;
            if (list != null)
            {
                list.Sort(SortByName);
            }
        }

        public void ExportDataTableDD(DataTable dt)
        {
            //this.Sort(new Comparison<Group>(SortByName));
            this.Sort();
            dt.Clear();           
            DataRow dr;

            dr = dt.NewRow();
            dr["decode"] = Groups.None;
            dr["code"] = "";
            dt.Rows.Add(dr); 
           
            foreach (Group g in this)
            {
                dr = dt.NewRow();
                dr["code"] = g.Name;
                dr["decode"] = g.Name;
                dt.Rows.Add(dr);
            }
           
            dt.DefaultView.Sort = "decode";
            //return dt;
        }


        public void ImportDataTable(DataTable dt)
        {            
            this.Clear();
            foreach (DataRow dr in dt.Rows)
            {                
                Group g = new Group();
                g.Name = (string)dr["Name"];
                g.SavePath = (string)dr["SavePath"];
                g.SyncPath = (string)RcData.RcIsNull(dr,"SyncPath","");
                g.BareSyncPath = (bool)dr["BareSyncPath"];
                g.PlaylistPath = (string)RcData.RcIsNull(dr, "PlaylistPath", "");
                g.PreSyncExeGroup = (string)RcData.RcIsNull(dr, "PreSyncExeGroup", "");
                g.PreSyncExeArgsGroup = (string)RcData.RcIsNull(dr, "PreSyncExeArgsGroup", "");
                g.SuppressSyncCopy = (bool)dr["SuppressSyncCopy"];
                if (!dr.IsNull("PlaylistCutoffDays"))
                    g.PlaylistCutoffDays = (int)dr["PlaylistCutoffDays"];
                this.Add(g);            
            }            
        }
    }//class
}//namespace
