﻿/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RcSoft.RcCommon;
using System.Data;
using System.IO;

namespace rcFeedMe
{
    public class History : System.Collections.ObjectModel.KeyedCollection<string, Media>
    {
        public const string XMLName = "History.xml";
        public readonly string XMLFullName = RcShared.GetExePath(false) + @"\" + History.XMLName;

        protected override string GetKeyForItem(Media m)
        {
            return m.PK;
        }

        private int SortByDate(Media m1, Media m2)
        {
            return m2.PubDate.CompareTo(m1.PubDate);
        }

        private int SortByDateRev(Media m1, Media m2)
        {
            return m1.PubDate.CompareTo(m2.PubDate);
        }

        public void Sort()
        {
            List<Media> list = base.Items as List<Media>;
            if (list != null)
            {
                list.Sort(SortByDate);
            }
        }

        public void SortReverse()
        {
            List<Media> list = base.Items as List<Media>;
            if (list != null)
            {
                list.Sort(SortByDateRev);
            }
        }
        
        public Media FindByFileName(string f)
        {
            var q = from m in this
                    where m.FullName == f                    
                    select m;
            foreach (Media m in q)
            {
                return m;
            }
            return null;
        }
        public void ExportDataTable(DataTable dt)
        {
            dt.Clear();
            foreach (Media m in this)
            {
                DataRow dr = dt.NewRow();
                dr["Name"] = m.FeedName;
                dr["Title"] = m.Title;
                dr["Summary"] = m.Summary;
                dr["Link"] = m.Link;                
                dr["FullName"] = m.FullName;
                dr["PubDate"] = m.PubDate;
                dr["Status"] = m.Status.ToString();
                dr["DoSave"] = m.DoSave;
                dr["TotalBytes"] = m.TotalBytes;
                dr["FileName"] = m.FileName;
                dr["Synced"] = m.Synced;
                dt.Rows.Add(dr);
            }
        }


        public void ImportDataTable(DataTable dt)
        {
            this.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                Media m = new Media();
                m.FeedName = (string)dr["Name"];
                m.Title = (string)dr["Title"];
                m.Summary = (string)dr["Summary"];
                m.Link = (string)dr["Link"];
                m.FullName = (string)dr["FullName"];
                m.PubDate = (System.DateTime)dr["PubDate"];
                string s = (string)dr["Status"];
                m.Status = (Media.StatusEnum)Enum.Parse(typeof(Media.StatusEnum),s,true);
                m.DoSave = (bool)dr["DoSave"];
                m.TotalBytes = (long)dr["TotalBytes"];
                m.FileName = (string)dr["FileName"];
                m.Synced = (bool)dr["Synced"];
                this.Add(m);
            }
            this.Sort();
        }


        public void Purge()
        {
            this.Clear();
            RcSerial.SaveToFile(this, this.XMLFullName);
        }

        public void Backup()
        {
            if (File.Exists(this.XMLFullName))
            {
                File.Copy(this.XMLFullName,this.XMLFullName + ".backup",true);
            }
        }

        public void Save(DataTable dt)
        {
            RcSerial.SaveToFile(this, this.XMLFullName);
            this.ExportDataTable(dt);
        }

        public void Search(DataTable dt, string s)
        {
            if (s == String.Empty)
            {
                dt.DefaultView.RowFilter = "";
            }
            else
            {
                s = s.Replace("'", "''");
                dt.DefaultView.RowFilter = "[Name] like '%" + s + "%' or [Title] like '%" + s + "%' or [FileName] like '%" + s + "%'";
            }
        }
    }
}
