﻿/*
 * Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RcSoft.RcCommon;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace rcFeedMe
{
    public class CleanupConfig
    {
        public const string XMLName = "CleanupConfig.xml";
        public readonly string XMLFullName = RcShared.GetExePath(false) + @"\" + CleanupConfig.XMLName;


        

        public struct ReplaceStruct         
        {
            [XmlAttribute("feedUrl")]
            public string feedUrl;

            public string oldString;
            public string newString;

            public ReplaceStruct(string o, string n, string f)
            {
                this.feedUrl = f;
                this.oldString = o;
                this.newString = n;
            }
        }

        public struct RemoveStruct
        {
            [XmlAttribute("feedUrl")]
            public string feedUrl;

            public string startString;
            public string endString;

            public RemoveStruct(string s, string e, string f)
            {
                this.feedUrl = f;
                this.startString = s;
                this.endString = e;
            }
        }

        public List<ReplaceStruct> ReplaceStrings = new List<ReplaceStruct>();
        public List<RemoveStruct> RemoveStrings = new List<RemoveStruct>();

        public void LoadDefaults()
        {            
            ReplaceStrings.Add(new ReplaceStruct(@"ï»¿","",""));
            RemoveStrings.Add(new RemoveStruct("<a href", @"</a>",""));
            RemoveStrings.Add(new RemoveStruct("{", "}",""));
            RemoveStrings.Add(new RemoveStruct("<script", "</script>",""));
            RemoveStrings.Add(new RemoveStruct("<template>", "</template>",""));
        }

        public static string CleanupXMLAll(string clean, string aFeedUrl)
        {
            string ret = CleanupXML(clean);
           if (!string.IsNullOrEmpty(aFeedUrl))
               ret = CleanupXML(ret,aFeedUrl);
            return ret;
        }

        public static string CleanupXML(string clean)
        {
            return CleanupXML(clean, string.Empty);
        }

        public static string CleanupXML(string clean, string aFeedUrl)
        {
            //try and cleanup crap feeds                
            CleanupConfig cc = new CleanupConfig();
            if (!File.Exists(cc.XMLFullName))
            {
                cc.LoadDefaults();
                RcSerial.SaveToFile(cc, cc.XMLFullName);
            }
            else
            {
                cc = (CleanupConfig)RcSerial.LoadFromFileFast(cc.GetType(), cc.XMLFullName);
            }  
          
            foreach (CleanupConfig.ReplaceStruct r in cc.ReplaceStrings)
            {
                if ((string.IsNullOrEmpty(aFeedUrl) && string.IsNullOrEmpty(r.feedUrl)) || 
                    (!string.IsNullOrEmpty(aFeedUrl) && !string.IsNullOrEmpty(r.feedUrl) && 
                    r.feedUrl == aFeedUrl))
                {
                    clean = clean.Replace(r.oldString, r.newString);
                }
            }
            foreach (CleanupConfig.RemoveStruct r in cc.RemoveStrings)
            {
                if ((string.IsNullOrEmpty(aFeedUrl) && string.IsNullOrEmpty(r.feedUrl)) ||
                    (!string.IsNullOrEmpty(aFeedUrl) && !string.IsNullOrEmpty(r.feedUrl) &&
                    r.feedUrl == aFeedUrl))
                {
                    clean = RcShared.RemoveText(r.startString, r.endString, clean);
                }
            }
            return clean;
        }
    }    
}
