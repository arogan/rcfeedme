﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RcSoft.RcCommon;

namespace rcFeedMe
{
    public class clsBase
    {
        protected Config Cfg = new Config();

        public clsBase()
        {
            Cfg = (Config)RcSerial.LoadFromFileFast(Cfg.GetType(), Cfg.XMLFullName);
        }
    }
}
