﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using RcSoft.RcCommon;
using System.Xml;
using System.ServiceModel.Syndication;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Configuration;
using rcFeedMe.Properties;

namespace rcFeedMe
{
    public partial class frmRcFeedMe : Form
    {
        private Groups groups = new Groups();
        private Feeds feeds = new Feeds();
        private Config cfg = new Config();
        private Queue queue = new Queue();
        private History history = new History();
        private History errors = new History();
        private int feedsRow = 0;
        //private bool isRefreshingAll = false;
        //private bool isQueueBusy = false;                
        private System.DateTime lastRun = Convert.ToDateTime("01/01/1950");
        private System.DateTime nextRun = Convert.ToDateTime("01/01/1950");        
        public const string APPVER = @"rcFeedMe v1.44f (11/8/2011)";
        private FormWindowState lastWindowState = FormWindowState.Normal;

        private enum StatusEnum
        {
            Idle,
            Feed,
            Queue,
            Sync
        }
        private StatusEnum Status = StatusEnum.Idle;
        private bool isBatchMode = false;
        private bool autoStart = false;
        private bool isLowMemMode = false;
        private bool autoStop = false;
        private bool skipSaveSettings = false;

        private Icon iconBusy = null;
        private Icon iconIdle = null;

        private string runMode = "";

        public frmRcFeedMe()
        {
        }

        public frmRcFeedMe(string[] args)
        {
            InitializeComponent();          
            //DataGridViewComboBoxColumn cbc = (DataGridViewComboBoxColumn)dgvFeeds.Columns["GroupName"];
            //cbc.DataSource = dsGroupDD.Tables[0].DefaultView;
            RcShared.WriteLog("Starting Application.");

            this.Load += new EventHandler(frmRcFeedMe_Load);            
            this.Resize += new EventHandler(frmRcFeedMe_Resize);                  
            notifyIcon1.DoubleClick += new EventHandler(notifyIcon1_DoubleClick);
            this.FormClosing += new FormClosingEventHandler(frmRcFeedMe_FormClosing);            
            this.txtSearch.KeyDown += new KeyEventHandler(txtSearch_KeyDown);                   

            dgvFeeds.CellContentClick += new DataGridViewCellEventHandler(dgvFeeds_CellContentClick);
            
            queue.ProgressChanged += new EventHandler(queue_ProgressChanged);
            queue.FinishedDownload += new EventHandler(queue_FinishedDownload);
            queue.FinishedFile += new EventHandler(queue_FinishedFile);

            this.Text = APPVER;
            notifyIcon1.Text = APPVER;

            iconIdle = new Icon(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("rcFeedMe.idle.ico"));
            iconBusy = new Icon(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("rcFeedMe.busy.ico"));
            notifyIcon1.Icon = iconIdle;

            SetToolTips();

            if (args.Length > 0)
            {
                string a = (string)args[0].ToLower();
                runMode = a;
                if (a == "1" || a == "auto")
                {
                    autoStart = true;                    
                }
                else if (a == "2" || a == "lite")
                {
                    autoStart = true;
                    isLowMemMode = true;                    
                }
                else if (a == "3" || a == "once")
                {
                    RcShared.WriteLog("Run once mode.");
                    autoStart = true;
                    autoStop = true;                    
                }
                else if (a == "4" || a == "hidden")
                {
                    //this.FormBorderStyle = FormBorderStyle.FixedToolWindow; //hide it from alt-tab               
                    //ShowInTaskbar = false;
                    //this.Hide();
                    RcShared.WriteLog("Run once mode (hidden).");                    
                    autoStart = true;
                    autoStop = true;   
                }
            }            
        }        

        void dgvFeeds_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView d = sender as DataGridView;
                if (d != null && e.RowIndex >= 0)
                {
                    var hc = d.Columns[e.ColumnIndex].HeaderCell as DataGridViewColumnHeaderCheckBoxCell;
                    DataGridViewCheckBoxCell cb = d[e.ColumnIndex, e.RowIndex] as DataGridViewCheckBoxCell;
                    if (cb != null && hc != null && (bool)cb.Value && hc.Checked)
                    {
                        hc.Checked = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                wl(ex.Message);
            }
        }

        void frmRcFeedMe_Load(object sender, EventArgs e)
        {

            //Configuration config =
            //    ConfigurationManager.OpenExeConfiguration(
            //    ConfigurationUserLevel.PerUserRoamingAndLocal);
            //Console.WriteLine("Local user config path: {0}", config.FilePath);

            //try
            //{
            //    Settings.Default.Reload();
            //}
            //catch (System.Configuration.ConfigurationErrorsException ex)
            //{ //(requires System.Configuration)
            //    string filename = ((ConfigurationErrorsException)ex.InnerException).Filename;

            //    if (MessageBox.Show("<ProgramName> has detected that your" +
            //                          " user settings file has become corrupted. " +
            //                          "This may be due to a crash or improper exiting" +
            //                          " of the program. <ProgramName> must reset your " +
            //                          "user settings in order to continue.\n\nClick" +
            //                          " Yes to reset your user settings and continue.\n\n" +
            //                          "Click No if you wish to attempt manual repair" +
            //                          " or to rescue information before proceeding.",
            //                          "Corrupt user settings",
            //                          MessageBoxButtons.YesNo,
            //                          MessageBoxIcon.Error) == DialogResult.Yes)
            //    {
            //        File.Delete(filename);
            //        Settings.Default.Reload();
            //        // you could optionally restart the app instead
            //    }
            //    else
            //        Process.GetCurrentProcess().Kill();
            //    // avoid the inevitable crash
            //}
            RcEncryptLite.InitDefaults();            

            addCheckboxToHeader(dgvFeeds,7);
            addCheckboxToHeader(dgvFeeds, 8);
            addCheckboxToHeader(dgvFeeds, 10);
            addCheckboxToHeader(dgvFeeds, 11);
            addCheckboxToHeader(dgvFeeds, 15);
            addCheckboxToHeader(dgvFeeds, 16);
            addCheckboxToHeader(dgvFeeds, 21);

            addCheckboxToHeader(dgvHistory, 8);
            addCheckboxToHeader(dgvHistory, 10);
            
            string str = RcShared.GetConfig("SplitterDistance");
            if (str != "")
                splitContainer1.SplitterDistance = int.Parse(str);           
        }

        private void addCheckboxToHeader(gfoidl.Windows.Forms.gfDataGridView dgv, int colNo)
        {
            DataGridViewColumnHeaderCheckBoxCell cbHeader = new DataGridViewColumnHeaderCheckBoxCell();
            cbHeader.CheckBoxClicked += new DataGridViewCheckBoxHeaderCellEvenHandler(cbHeader_CheckBoxClicked);
            string h = dgv.Columns[colNo].HeaderText;
            string tt = dgv.Columns[colNo].ToolTipText;
            dgv.Columns[colNo].HeaderCell = cbHeader;
            dgv.Columns[colNo].HeaderText = h;
            dgv.Columns[colNo].ToolTipText = tt;
        }

        void cbHeader_CheckBoxClicked(object sender, DataGridViewCheckBoxHeaderCellEventArgs e)
        {
            DataGridViewColumnHeaderCheckBoxCell h = sender as DataGridViewColumnHeaderCheckBoxCell;
            DataGridView dgv = h.DataGridView;
            string c = "";

            switch (h.DataGridView.Name)
            {
                case "dgvFeeds":
                    switch (h.ColumnIndex)
                    {
                        case 7:
                            c = "PrependDate";
                            break;
                        case 8:
                            c = "Prepend";
                            break;
                        case 10:
                            c = "DoSync";
                            break;
                        case 11:
                            c = "DoCacheFix";
                            break;
                        case 15:
                            c = "DoDecode";
                            break;
                        case 16:
                            c = "PrependShortDate";
                            break;
                        case 21:
                            c = "SuppressSyncCopy";
                            break;
                    }
                    break;
                case "dgvHistory":
                    switch (h.ColumnIndex)
                    {
                        case 8:
                            c = "DoSave";
                            break;
                        case 10:
                            c = "Synced";
                            break;
                    }
                    break;
            }
            

            foreach (DataGridViewRow r in dgv.Rows)            
            {                
                DataRowView drv = (DataRowView)r.DataBoundItem;
                if (drv != null && drv[c] != null)
                {
                    drv[c] = e.Checked;                    
                }
            }
            dgv.RefreshEdit();
            dgv.Refresh();
        }
       
        private void SetToolTips()
        {
            toolTip1.SetToolTip(txtFreq, "Specify how often you want the fees to be automatically processed in minutes.");
            toolTip1.SetToolTip(lblFreq, "Specify how often you want the fees to be automatically processed in minutes.");
            toolTip1.SetToolTip(txtDefPath, "If a feed doesn't have a path specified and group is set to none, then this path will be used as the base path.");
            toolTip1.SetToolTip(lblDefPath, "If a feed doesn't have a path specified and group is set to none, then this path will be used as the base path.");
            toolTip1.SetToolTip(txtSyncPath, "If a feed doesn't have a path specified and group is set to none, then this path will be used as the base Sync path.");
            toolTip1.SetToolTip(lblSyncPath, "If a feed doesn't have a path specified and group is set to none, then this path will be used as the base Sync path.");
            toolTip1.SetToolTip(txtDefKeepCount, "Default keep count used when importing feeds from an opml file.");
            toolTip1.SetToolTip(lblDefKeepCount, "Default keep count used when importing feeds from an opml file.");
            toolTip1.SetToolTip(txtThreadScan, "Number of threads to use when refreshing the rss feeds. (Maximum 16)");
            toolTip1.SetToolTip(lblThreadScan, "Number of threads to use when refreshing the rss feeds. (Maximum 16)");
            toolTip1.SetToolTip(txtThreadDL, "Number of threads to use when downloading files.  I would recommend 2.  Any higher and some sites limit the number of simultaneous downloads and could produce false failed downloads. (Maximum 6)");
            toolTip1.SetToolTip(lblThreadDL, "Number of threads to use when downloading files.  I would recommend 2.  Any higher and some sites limit the number of simultaneous downloads and could produce false failed downloads. (Maximum 6)");
            toolTip1.SetToolTip(txtScanStall, "Number of seconds that elapse of inactivity while a feed is being refreshed.  If the feed refresh stalls for this many seconds, it will be considered an error.");
            toolTip1.SetToolTip(lblScanStall, "Number of seconds that elapse of inactivity while a feed is being refreshed.  If the feed refresh stalls for this many seconds, it will be considered an error.");
            toolTip1.SetToolTip(txtDLStall, "Number of seconds that elapse of inactivity while a file is being downloaded.  If the download stalls for this many seconds, it will be considered an error.");
            toolTip1.SetToolTip(lblDLStall, "Number of seconds that elapse of inactivity while a file is being downloaded.  If the download stalls for this many seconds, it will be considered an error.");
            toolTip1.SetToolTip(txtScanTimeout, "If the total time for a feed refresh takes longer than this many seconds, it is considered an error.");
            toolTip1.SetToolTip(lblScanTimeout, "If the total time for a feed refresh takes longer than this many seconds, it is considered an error.");
            toolTip1.SetToolTip(txtDLTimeout, "If the total time for a file download takes longer than this many seconds, it is considered an error.");
            toolTip1.SetToolTip(lblDLTimeout, "If the total time for a file download takes longer than this many seconds, it is considered an error.");
            toolTip1.SetToolTip(txtScanRetry, "When an error occurs during a feed refresh, retry this many times before giving up.");
            toolTip1.SetToolTip(lblScanRetry, "When an error occurs during a feed refresh, retry this many times before giving up.");
            toolTip1.SetToolTip(txtDLRetry, "When an error occurs during a file download, retry this many times before giving up.");
            toolTip1.SetToolTip(lblDLRetry, "When an error occurs during a file download, retry this many times before giving up.");
            toolTip1.SetToolTip(txtTimezones, "Used to help parse Publish Date.  Sometimes it helps to strip out the time zone.");
            toolTip1.SetToolTip(lblTimeZones, "Used to help parse Publish Date.  Sometimes it helps to strip out the time zone.");
            toolTip1.SetToolTip(txtDateFormats, "Used to help parse Publish Date.  Specify the .net Custom Date and Time Format string.");
            toolTip1.SetToolTip(lblDateFormats, "Used to help parse Publish Date.  Specify the .net Custom Date and Time Format string.");
        }

        private void timerDelayStart_Tick(object sender, EventArgs e)
        {
            timerDelayStart.Stop();
            startup();
        }

        public void startup()
        {
            if (RcShared.PrevInstance())
            {
                RcShared.WriteLog("Another instance already running.  Shutting down.");
                skipSaveSettings = true;
                this.Close();
                return;
            }
            if (isLowMemMode)
            {
                dgvFeeds.Enabled = false;
                dgvFeed.Enabled = false;
                dgvQueue.Enabled = false;
                dgvHistory.Enabled = false;
                dgvErrors.Enabled = false;
                dgvGroups.Enabled = false;
                tableLayoutPanel1.Enabled = false;
                tsbRefreshAll.Enabled = false;
                mnuExportOpml.Enabled = false;
                mnuImportOpml.Enabled = false;
                mnuDebug.Enabled = false;
                btnSync.Enabled = false;
                tsbSaveReloadSettings.Enabled = false;
                tsbSearchHistory.Enabled = false;
                txtSearch.Enabled = false;
            }
            else
            {
                LoadSettings();
            }
            //RefreshAll();
            SetStatus(StatusEnum.Idle);
            if (autoStart)
            {
                autoStart = false;
                this.WindowState = FormWindowState.Minimized;
                StartSched();                
            }            
        }

        void frmRcFeedMe_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Status != StatusEnum.Idle && !autoStop)
            {
                DialogResult dr = MessageBox.Show("rcFeedMe is currently busy.  Are you sure you want to close now?" + RcShared.CRLF + RcShared.CRLF + "If the queue is being processed it is recommended you right click and stop all downloads first before closing rcFeedMe.", "Closing rcFeedMe", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            formIsClosing();
        }

        private void formIsClosing()
        {
            RcShared.WriteConfig("SplitterDistance", splitContainer1.SplitterDistance.ToString());
            CloseApp();
        }
              
        private bool IsBusy()
        {
            if (this.Status != StatusEnum.Idle)
            {
                if (Status == StatusEnum.Queue)
                {
                    wl("rcFeedMe is currently processing the download queue which prevents you from doing your current action.  Please try again later.");
                }
                else if (Status == StatusEnum.Feed)
                {
                    wl("rcFeedMe is currently refreshing all the feeds which prevents you from doing your current action.  Please try again later.");
                }
                else if (Status == StatusEnum.Sync)
                {
                    wl("rcFeedMe is currently syncing files which prevents you from doing your current action.  Please try again later.");
                }
                else
                {
                    wl("rcFeedMe is currently busy which prevents you from doing your current action.  Please try again later.");
                }
                return true;
            }
            return false;
        }

        private void SaveSettings()
        {
            if (IsBusy() || isLowMemMode)
            {
                return;
            }
            try
            {
                wl("Saving Settings....");
                //Config
                UpdateConfig();
                RcSerial.SaveToFile(cfg, cfg.XMLFullName);

                //feeds
                dgvFeeds.EndEdit();
                if (dgvFeeds.CurrentRow != null)
                {
                    DataRowView drvF = (DataRowView)dgvFeeds.CurrentRow.DataBoundItem;
                    if (drvF != null)
                        drvF.EndEdit();
                }                
                DataTable dtF = dsFeeds.Tables[0];                
                if (dtF != null)
                {
                    feeds.ImportDataTable(dtF);
                }
                feeds.EncryptPw();
                RcSerial.SaveToFile(feeds, feeds.XMLFullName);
                feeds.DecryptPw();

                //groups
                dgvGroups.EndEdit();
                if (dgvGroups.CurrentRow != null)
                {
                    DataRowView drv = (DataRowView)dgvGroups.CurrentRow.DataBoundItem;
                    if (drv != null)
                        drv.EndEdit();
                }
                DataTable dt = dsGroups.Tables[0];
                if (dt != null)
                {
                    groups.ImportDataTable(dt);
                }
                RcSerial.SaveToFile(groups, groups.XMLFullName);

                //history
                dgvHistory.EndEdit();
                if (dgvHistory.CurrentRow != null)
                {
                    DataRowView drv = (DataRowView)dgvHistory.CurrentRow.DataBoundItem;
                    if (drv != null)
                        drv.EndEdit();
                }
                DataTable dth = dsHistory.Tables[0];
                if (dth != null)
                {
                    history.ImportDataTable(dth);
                }
                RcSerial.SaveToFile(history, history.XMLFullName);
            }
            catch (System.Exception ex)
            {
                wl(ex.Message);
                return;
            }
            wl("Settings saved.");
        }

        private void LoadSettings()
        {
            if (IsBusy())
            {               
                return;
            }
            try
            {
                wl("Loading Settings....");
                RcSharedEx.DoEvents();
                cfg = (Config)RcSerial.LoadFromFileFast(cfg.GetType(), cfg.XMLFullName);
                groups = (Groups)RcSerial.LoadFromFileFast(groups.GetType(), groups.XMLFullName);
                feeds = (Feeds)RcSerial.LoadFromFileFast(feeds.GetType(), feeds.XMLFullName);
                feeds.DecryptPw();
                feeds.Cfg = cfg;
                feeds.Grps = groups;           
                feeds.FinishedRefresh +=new EventHandler(feeds_FinishedRefresh);
                feeds.ProgressChanged += new EventHandler(feeds_ProgressChanged);
                //foreach (Feed f in feeds)
                //{
                //    f.FinishedRefresh +=new EventHandler(f_FinishedRefresh);
                //}
                feeds.WireupEvents();                                
                history = (History)RcSerial.LoadFromFileFast(history.GetType(), history.XMLFullName);
                history.Sort();
            }
            catch (System.Exception ex)
            {
                wl(ex.Message);
                return;
            }
            finally
            {
                BindConfig();

                groups.ExportDataTable(dsGroups.Tables[0]);                
                groups.ExportDataTableDD(dsGroupDD.Tables[0]);
                feeds.ExportDataTable(dsFeeds.Tables[0]);
                dsFeeds.Tables[0].Columns["KeepCount"].DefaultValue = cfg.KeepCount;
                history.ExportDataTable(dsHistory.Tables[0]);
                dsFeed.Tables[0].Clear();
                //dgvFeeds.DataSource = feeds.ExportDataTable();
                //dgvGroups.DataSource = groups.ExportDataTable();
            }
            wl("Settings loaded.");
        }        
        
        private void BindConfig()
        {
            txtFreq.Text = cfg.Freq.ToString();
            txtDefPath.Text = cfg.SavePath;
            txtDefKeepCount.Text = cfg.KeepCount.ToString();
            txtThreadDL.Text = cfg.DLThread.ToString();
            txtThreadScan.Text = cfg.ScanThread.ToString();
            txtDLStall.Text = cfg.DLStall.ToString();
            txtScanStall.Text = cfg.ScanStall.ToString();
            txtDLRetry.Text = cfg.DLRetry.ToString();
            txtScanRetry.Text = cfg.ScanRetry.ToString();
            txtDLTimeout.Text = cfg.DLTimeout.ToString();
            txtScanTimeout.Text = cfg.ScanTimeout.ToString();
            txtSyncPath.Text = cfg.SyncPath;
            txtTimezones.Text = cfg.TimeZones;
            txtDateFormats.Text = cfg.DateFormats;
            chkDupErrors.Checked = cfg.LogDupErrors;
        }

        private void UpdateConfig()
        {
            cfg.Freq = Convert.ToInt32(txtFreq.Text);
            cfg.SavePath = txtDefPath.Text;
            cfg.KeepCount = Convert.ToInt32(txtDefKeepCount.Text);
            cfg.DLThread = Convert.ToInt32(txtThreadDL.Text);
            cfg.ScanThread = Convert.ToInt32(txtThreadScan.Text);
            cfg.DLStall = Convert.ToInt32(txtDLStall.Text);
            cfg.ScanStall = Convert.ToInt32(txtScanStall.Text);
            cfg.DLRetry = Convert.ToInt32(txtDLRetry.Text);
            cfg.ScanRetry = Convert.ToInt32(txtScanRetry.Text);
            cfg.DLTimeout = Convert.ToInt32(txtDLTimeout.Text);
            cfg.ScanTimeout = Convert.ToInt32(txtScanTimeout.Text);
            cfg.SyncPath = txtSyncPath.Text;
            cfg.TimeZones = txtTimezones.Text;
            cfg.DateFormats = txtDateFormats.Text;
            cfg.LogDupErrors = chkDupErrors.Checked;
        }

        public void ImportOpml()
        {
            if (IsBusy())
                return;
            int c = 0;
            DialogResult ret = openFileDialog1.ShowDialog();
            if (ret != DialogResult.Cancel)
            {
                DataTable dt = dsFeeds.Tables[0];                
                //dt.Clear();
                string f = openFileDialog1.FileName;
                Opml o = Opml.Parse(f);
                foreach (OpmlOutline ol in o.Outlines)
                {
                    if (!dt.Rows.Contains(ol.Title))
                    {
                        DataRow dr = dt.NewRow();
                        dr["Name"] = ol.Title;
                        dr["Url"] = ol.XmlUrl;
                        dr["KeepCount"] = cfg.KeepCount;
                        dt.Rows.Add(dr);
                        c++;
                    }
                }
                //dgvFeeds.DataSource = dt;
                wl(c.ToString() + " feeds imported.");
            }
        }

        public void ExportOpml()
        {
            DialogResult ret = saveFileDialog1.ShowDialog();
            if (ret != DialogResult.Cancel)
            {
                Opml o = new Opml();
                o.Title = "rcFeedme Opml Export";
                o.DateCreated = System.DateTime.Now;
                o.DateModified = System.DateTime.Now;               
                foreach (DataRow dr in dsFeeds.Tables[0].Rows)
                {
                    OpmlOutline ol = new OpmlOutline();
                    ol.Title = (string)dr["Name"];
                    ol.Text = ol.Title;
                    ol.XmlUrl = new Uri((string)dr["Url"]);
                    o.AddOutline(ol);                    
                }
                string f = saveFileDialog1.FileName;
                o.Save(f);                
                wl(o.ToString() + " feeds exported to " + f);
            }
        }
                
        private void wl(string msg)
        {
            RcShared.WriteLog(msg);
            if (msg.Length > 125)
                msg = msg.Substring(0, 125);
            toolStripStatusLabel1.Text = msg;            
        }

        private void w2(string msg)
        {
            tsStatus2.Text = msg;
        }

        private void SetStatus(StatusEnum se)
        {
            Status = se;
            if (isLowMemMode)
                w2(Status.ToString() + " [Lite]");           
            else
                w2(Status.ToString());

            if (Status == StatusEnum.Idle)
            {
                if (notifyIcon1.Icon != null)
                    notifyIcon1.Icon = iconIdle;
            }
            else
            {
                if (notifyIcon1.Icon != null)
                    notifyIcon1.Icon = iconBusy;
            }
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabControl tc = (TabControl)sender;
            switch (tc.SelectedTab.Name)
            {
                case "tpLog":
                    RefreshLog();
                    break;
                case "tpErrorLog":
                    RefreshErrorLog();
                    break;
                case "tpQueue":
                    if (Status != StatusEnum.Queue)
                        queue.ExportDataTable(dsQueue.Tables[0]);
                    break;
                case "tpHistory":
                    if (Status != StatusEnum.Queue)
                        RefreshHistory();
                    break;
                case "tpErrors":
                    if (Status != StatusEnum.Queue)
                        errors.ExportDataTable(dsErrors.Tables[0]);
                    break;
            }
        }

        private void SetTitle()
        {
            SetTitle("");
        }
        private void SetTitle(string s)
        {
            string sep1 = "; ";
            string sep2 = " - ";

            if (s == String.Empty)
            {
                sep1 = "";
                sep2 = "";
            }
            
            if (timerSched.Enabled)
            {
                notifyIcon1.Text = "rcFeedMe - Next run: " + nextRun.ToString() + sep1 + s;
                this.Text = APPVER + " - Next run: " + nextRun.ToString() + sep1 + s;
            }
            else
            {
                notifyIcon1.Text = "rcFeedMe" + sep2 + s;
                this.Text = APPVER + sep2 + s;
            }
        }

        public void CloseApp()
        {
            RcShared.WriteLog("Closing Application");
            StopSched();            
            if (!skipSaveSettings)
            {
                SaveSettings();
            }
        }

        private int RemoveSelectedRows(DataGridView dgv)
        {
            int cnt = 0;
            foreach (DataGridViewRow r in dgv.SelectedRows)
            {
                if (!r.IsNewRow)
                {
                    dgv.Rows.Remove(r);
                    cnt++;
                }
            }
            wl("Rows removed: " + cnt.ToString());
            return cnt;
        }

        private void SyncCount()
        {
            try
            {
                int i = Sync(true);
                if (i > 0)
                {
                    SetTitle("Sync: " + i.ToString());
                    btnSync.Text = "Sync (" + i.ToString() + ")";
                }
                else
                {
                    SetTitle();
                    btnSync.Text = "Sync";
                }
            }
            catch (System.Exception ex)
            {
                wl("WARNING: " + ex.Message);
            }
        }
        private int Sync()
        {
            return Sync(false);
        }
        private int Sync(bool countOnly)
        {            
            if (!countOnly && IsBusy())
                return -1;

            try
            {
                int cnt = 0;

                if (!countOnly)
                {
                    tabMain.SelectTab(tpFeeds);
                    SetStatus(StatusEnum.Sync);
                    RefreshHistory();
                }
                History historySync = new History();

                foreach (Feed f in feeds)
                {
                    if (f.DoSync)
                    {
                        Group g = null;
                        if (f.GroupName != "")
                            g = groups[f.GroupName];
                        string finalPath = f.Resolvepath(g);

                        DirectoryInfo di = new DirectoryInfo(finalPath);
                        if (di.Exists)
                        {
                            FileInfo[] fic = di.GetFiles();
                            foreach (FileInfo fi in fic)
                            {
                                Media m = history.FindByFileName(fi.FullName);
                                if (m != null)
                                {
                                    if (!m.Synced)
                                    {
                                        if (!countOnly)
                                        {
                                            m.ResolveSyncFullName(f, g);
                                            m.FeedName = f.Name;
                                            m.PostDLExe = f.PostDLExe;
                                            m.PostDLArgs = f.PostDLArgs;
                                            if (f.group == null)
                                                m.GroupName = "";
                                            else   
                                                m.GroupName = f.group.Name;
                                            //
                                            m.SuppressSyncCopy = f.SuppressSyncCopy;
                                            if (g != null)
                                            {
                                                m.SynchPath = g.SyncPath;
                                                m.PreSyncExe = g.PreSyncExeGroup;
                                                m.PreSyncArgs = g.PreSyncExeArgsGroup;
                                                m.SuppressSyncCopy = g.SuppressSyncCopy;
                                            }
                                            if (f.SyncPath!="")
                                                m.SynchPath = f.SyncPath;
                                            if (f.PreSyncExe!="")
                                                m.PreSyncExe = f.PreSyncExe;
                                            if (f.PreSyncArgs!="")
                                                m.PreSyncArgs = f.PreSyncArgs;
                                        }
                                        if (historySync.FirstOrDefault(p => p.PK == m.PK) == null)
                                            historySync.Add(m);
                                    }
                                }
                            }
                        }                        
                    }
                }

                if (countOnly)
                    return historySync.Count;

                if (historySync.Count > 0)
                {
                    progBar.Value = 0;
                    progBar.Maximum = historySync.Count;
                    foreach (Media m in historySync)
                    {
                        cnt++;
                        wl("Syncing: (" + cnt.ToString() + @"/" + historySync.Count.ToString() + ") " + m.FileName + " -> " + m.SyncFullName);
                        RcSharedEx.DoEvents();
                        if (cnt < progBar.Maximum)
                            progBar.Value = cnt;

                        //pre sync process
                        if (!string.IsNullOrEmpty(m.PreSyncExe))
                        {
                            string args = "";
                            try
                            {
                                if (string.IsNullOrEmpty(m.PreSyncArgs))
                                    args = "";
                                else
                                    args = m.PreSyncArgs;

                                args = args.Replace(Config.tokenFilename, m.FileName);
                                args = args.Replace(Config.tokenFullname, m.FullName);
                                args = args.Replace(Config.tokenFeedname, m.FeedName);
                                args = args.Replace(Config.tokenSavePath, Path.GetDirectoryName(m.FullName));
                                args = args.Replace(Config.tokenDate, m.PubDate.ToString("yyyyMMdd"));
                                args = args.Replace(Config.tokenSyncPath, m.SynchPath);
                                args = args.Replace(Config.tokenGroupname, m.GroupName);

                                RcShared.WriteLog("Pre Sync exe = " + m.PreSyncExe + "; args = " + args);
                                RcProcess.RunSyncHide(m.PreSyncExe, args);
                            }
                            catch (System.Exception pex)
                            {
                                RcShared.WriteError("Pre Sync process failed: " + m.PreSyncExe + "; args = " + args + "; " + pex.Message);
                            }
                        }

                        if (!m.SuppressSyncCopy)
                            File.Copy(m.FullName, m.SyncFullName, true);
                        else
                            RcShared.WriteLog("Sync copy was suppressed: " + m.FullName);

                        m.Synced = true;
                    }
                    wl("Finished Syncing: " + cnt.ToString());
                    progBar.Value = 0;
                    history.Save(dsHistory.Tables[0]);
                }
                else
                {
                    wl("No files need to be synced.");
                }
                btnSync.Text = "Sync";
                SetTitle();
                return cnt;
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
                return -1;
            }
            finally
            {
                SetStatus(StatusEnum.Idle);
            }
        }

        public void TouchDates(List<FileInfo> files)
        {
            foreach (FileInfo f in files)
            {
                if (f.Exists)
                {
                    Media m = history.FindByFileName(f.FullName);
                    if (m != null)
                    {
                        f.LastWriteTime = m.PubDate;
                    }
                }
            }
        }

        public void CreatePlaylists()
        {
            if (IsBusy())
                return;
            try
            {
                wl("Create playlists");
                SaveSettings();
                LoadSettings();
                var groupList = groups.Where(g => !string.IsNullOrEmpty(g.PlaylistPath) && g.PlaylistCutoffDays.HasValue);
                foreach (var g in groupList)
                {
                    List<FileInfo> topFiles = new List<FileInfo>();
                    List<FileInfo> olderFiles = new List<FileInfo>();

                    var feedList = feeds.Where(f => f.group.Name == g.Name && f.Priority.HasValue).OrderBy(o => o.Priority);
                    foreach (Feed f in feedList)
                    {
                        wl("(playlist)Processing feed: " + f.Name);
                        string finalPath = f.Resolvepath(g);
                        DirectoryInfo d = new DirectoryInfo(finalPath);
                        List<FileInfo> fic = new List<FileInfo>(d.GetFiles("*.mp3*"));
                        TouchDates(fic);
                        fic = new List<FileInfo>(d.GetFiles("*.mp3*"));

                        //FileInfoCollection fic = new FileInfoCollection(d.GetFiles("*.mp3*"));
                        if (fic.Count > 0)
                        {
                            if (g.PlaylistCutoffDays > 0)
                            {
                                var ficOrdered = fic.Where(x => x.LastWriteTime >= System.DateTime.Now.AddDays((double)g.PlaylistCutoffDays * -1));
                                fic = new List<FileInfo>(ficOrdered);
                            }
                            fic = new List<FileInfo>(fic.OrderByDescending(x => x.LastWriteTime));
                            if (fic.Count > 0)
                            {
                                topFiles.Add(fic[0]);
                                for (int i = 1; i < fic.Count; i++)
                                {
                                    olderFiles.Add(fic[i]);
                                }
                            }
                        }
                    }
                    topFiles.AddRange(olderFiles);
                    string m3u = genPL(topFiles);
                    RcShared.OWriteLog(g.PlaylistPath, m3u);
                    wl(g.PlaylistPath + " created with " + topFiles.Count.ToString() + " mp3 files.");
                }
                wl("Finished creating playlists");
            }
            catch (System.Exception ex)
            {
                wl("[ERROR] (playlist) msg = " + ex.Message + "; stack trace = " + ex.StackTrace);
            }
        }

        private string genPL(List<FileInfo> files)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"#EXTM3U");
            foreach (FileInfo f in files)
            {
                string title = f.Name;                                
                int duration = 0;
                try
                {
                    const string ORI = "[ORI]";
                    TagLib.File file = TagLib.File.Create(f.FullName);                
                    duration = int.Parse(Math.Round(file.Properties.Duration.TotalSeconds,0).ToString());
                    if (string.IsNullOrEmpty(file.Tag.Comment) || !file.Tag.Comment.StartsWith(ORI))
                    {
                        title = file.Tag.Title;
                        if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(title.Trim()))
                            title = f.Name;
                        file.Tag.Comment = "[ORI]" + title;
                        file.Save();
                    }
                    title = file.Tag.Comment.Replace(ORI, "");
                    file.Tag.Title = f.LastWriteTime.Month.ToString() + @"/" + f.LastWriteTime.Day.ToString() + "-" + title;
                    file.Save();
                    List<FileInfo> tempList = new List<FileInfo>();
                    tempList.Add(f);
                    TouchDates(tempList);                    
                }
                catch (System.Exception ex)
                {
                    RcShared.WriteLog("Failed to get id3 tag information: " + f.FullName + "; " + ex.Message);
                }
                string line = @"#EXTINF:" + duration.ToString() + "," + f.LastWriteTime.Month.ToString() + @"/" + f.LastWriteTime.Day.ToString() + "-" + title;
                sb.AppendLine(line);                
                string str = f.FullName;
                sb.AppendLine(str);                
            }
            return sb.ToString();
        }

        #region Main Menu/Tray
        private void mnuSaveSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }

        private void mnuLoadSettings_Click(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void mnuRunBatch_Click(object sender, EventArgs e)
        {
            //tabMain.SelectTab(tpQueue);
            RunBatch();
        }

        private void mnuTrayStart_Click(object sender, EventArgs e)
        {
            StartSched();
        }

        private void mnuTrayStop_Click(object sender, EventArgs e)
        {
            StopSched();
        }

        private void mnuTrayExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mnuAbout_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(APPVER + Environment.NewLine + "by ARogan" + Environment.NewLine + @"http://blog.arogan.com", "rcFeedMe", MessageBoxButtons.OK, MessageBoxIcon.Information);
            frmAbout about = new frmAbout();
            about.ShowDialog();
        }

        private void mnuImportOpml_Click(object sender, EventArgs e)
        {
            ImportOpml();
        }

        private void mnuExportOpml_Click(object sender, EventArgs e)
        {
            ExportOpml();
        }

        private void tsbRefreshAll_Click(object sender, EventArgs e)
        {
            RefreshAll();
        }

        void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            restore();
        }

        private void mnuRestore_Click(object sender, EventArgs e)
        {
            restore();
        }

        private void restore()
        {
            if (runMode == "4" || runMode == "hidden")
                return;
            this.Show();
            ShowInTaskbar = true;
            WindowState = lastWindowState;
            this.FormBorderStyle = FormBorderStyle.Sizable;            
        }

        void frmRcFeedMe_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.FormBorderStyle = FormBorderStyle.FixedToolWindow; //hide it from alt-tab               
                ShowInTaskbar = false;
                this.Hide();
            }
            else
            {
                lastWindowState = WindowState;
            }
        }

        private void tsbSaveReloadSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
            LoadSettings();

        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            Sync();
        }

        private void tsbSearchHistory_Click(object sender, EventArgs e)
        {
            SearchHistory();
        }

        private void SearchHistory()
        {
            tabMain.SelectTab(tpHistory);
            history.Search(dsHistory.Tables[0], txtSearch.Text);
            dgvHistory.DataMember = "";
            dgvHistory.DataSource = dsHistory.Tables[0].DefaultView;
            wl("Finished searching history: " + dsHistory.Tables[0].DefaultView.Count.ToString());
        }

        void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SearchHistory();
            }
        }

        private void mnuDebugSyncCount_Click(object sender, EventArgs e)
        {
            SyncCount();
        }

        private void clearAllSyncFlagsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Media m in history)
            {
                m.Synced = false;
            }
            history.Save(dsHistory.Tables[0]);
        }
      

        #endregion Main Menu

        #region Feeds

        void dgvFeeds_CellEndEdit(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {      
            try
            {
                int i = e.ColumnIndex;
                if (i != 1) return;
                string cc = (string)RcData.RcIsNull(dgvFeeds.CurrentCell.Value, "");
                string c0 = (string)RcData.RcIsNull(dgvFeeds.CurrentRow.Cells[0].Value, "");
                if (i == 1 && cc != String.Empty && c0 == String.Empty)
                {
                    
                        string s = Feed.GetTitle(cc);
                        dgvFeeds.CurrentRow.Cells[0].Value = s;
                        wl("Feed name set to: " + s);
                }
            }
            catch (System.Exception ex)
            {
                wl("Warning: Unable to get feed title. Please type the feed name. " + ex.Message);
            }
            
        }

        void dgvFeeds_CurrentCellChanged(object sender, System.EventArgs e)
        {
            if (Status == StatusEnum.Feed)
            {
                return;
            }
            if (dgvFeeds.CurrentRow == null)
                return;
            if (dgvFeeds.CurrentRow.IsNewRow)
                return;
            if (dgvFeeds.CurrentRow.Index != feedsRow)
            {
                FeedsRowChanged();
            }            
        }

        private void FeedsRowChanged()
        {
            try
            {
                feedsRow = dgvFeeds.CurrentRow.Index;
                DataRowView drv = (DataRowView)dgvFeeds.CurrentRow.DataBoundItem;
                string name = (string)drv["Name"];
                if (name == String.Empty)
                    return;
                if (!feeds.Contains(name))
                    return;
                Feed f = feeds[name];
                if (f.Status != Feed.StatusEnum.Init && f.Status != Feed.StatusEnum.Ignore)
                {
                    FilterFeed(dsFeed.Tables[0], f);
                    if (f.Status == Feed.StatusEnum.Error)
                        wl("[ERROR] " + f.Name + ": " + dsFeed.Tables[0].DefaultView.Count.ToString());
                    else
                        wl(f.Name + ": " + dsFeed.Tables[0].DefaultView.Count.ToString());
                    dgvFeed.DataSource = dsFeed.Tables[0].DefaultView;
                }
                else
                {
                    FilterFeed(dsFeed.Tables[0], f);
                }
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
        }

        private int FilterFeed(DataTable dt, Feed f)
        {
            dt.DefaultView.RowFilter = "Name = '" + f.Name.Replace("'", "''") + "'";
            return dt.DefaultView.Count;
        }

        private void dgvFeed_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                AddMedia(dgvFeed.CurrentRow);
                dgvFeed.Refresh();
            }
        }

        private bool AddMedia(DataGridViewRow row)
        {
            try
            {
                if (Status == StatusEnum.Feed)
                {
                    wl("Can't add media while refreshing feeds.");
                    return false;
                }
                DataRowView drv = (DataRowView)row.DataBoundItem;
                string pk = (string)drv["PK"];
                if ((bool)drv["InQueue"] == true)
                {
                    wl("File is already in queue: " + pk);
                    return false;
                }

                string feedName = (string)drv["Name"];
                Feed f = feeds[feedName];
                Media m = f.MediaCollection[pk];
                Group g = null;
                if (f.GroupName != String.Empty)
                {
                    g = groups[f.GroupName];
                }                
                queue.AddMedia(m, f, g, cfg, true);
                drv["InQueue"] = true;

                if (Status == StatusEnum.Queue)
                {
                    DataTable dt = dsQueue.Tables[0];
                    DataRow dr = dt.NewRow();
                    dr["Name"] = m.FeedName;
                    dr["Title"] = m.Title;
                    dr["Link"] = m.Link;
                    dr["FileName"] = m.FullName;
                    dr["dtPub"] = m.PubDate;
                    dr["Status"] = m.Status.ToString();
                    dr["Bytes"] = m.BytesText;
                    dr["Progress"] = m.PercentComplete;
                    dr["PK"] = m.PK;
                    dt.Rows.Add(dr);
                    m.dr = dr;
                    progBar.Maximum++;
                    wl(pk + " was added to queue.  Queue is currently being processed.");
                }
                else
                {
                    wl(pk + " was added to queue.");
                }

                return true;
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
                return false;
            }
        }

        private void mnuFeedAddSelected_Click(object sender, EventArgs e)
        {
            if (Status == StatusEnum.Feed)
            {
                wl("Can't add media while refreshing feeds.");
                return;
            }
            int c = 0;

            foreach (DataGridViewRow r in dgvFeed.SelectedRows)
            {
                if (AddMedia(r))
                    c++;
            }
            dgvFeed.Refresh();
            wl("Added " + c.ToString() + " files to queue.");
        }

        private void mnuFeedsRemove_Click(object sender, EventArgs e)
        {
            if (!IsBusy())
            {
                RemoveSelectedRows(dgvFeeds);
            }
        }

        private void mnuFeedsRefreshSelected_Click(object sender, EventArgs e)
        {
            if (IsBusy())
                return;

            if (dgvFeeds.SelectedRows.Count == 0)
                return;

            foreach (Feed f in feeds)
            {
                if (f.Status != Feed.StatusEnum.Error)
                {
                    f.Status = Feed.StatusEnum.Ignore;
                }
            }


            int cnt = 0;
            foreach (DataGridViewRow r in dgvFeeds.SelectedRows)
            {
                if (!r.IsNewRow)
                {
                    DataRowView drv = (DataRowView)r.DataBoundItem;
                    string feedName = (string)drv["Name"];
                    Feed f = feeds[feedName];
                    f.Status = Feed.StatusEnum.Init;
                    cnt++;
                }
            }
            RefreshSelected(cnt);
            
        }

        private void RefreshSelected(int selCnt)
        {
            if (IsBusy())
            {
                return;
            }
            dgvFeeds.CurrentCell = null;
            progBar.Value = 0;
            progBar.Maximum = selCnt;
            wl("Refreshing Selected feeds: " + selCnt.ToString());
            SetStatus(StatusEnum.Feed);
            feeds.DumpRawXml = mnuDumpXml.Checked;
            if (!feeds.RefreshAll(false))
            {
                wl("Refresh Selected was aborted");
            }
        }

        private void mnuFeedsStopRefresh_Click(object sender, EventArgs e)
        {
            feeds.StopAll();
        }

        #endregion Feeds

        #region Queue
        private void mnuQueueBuild_Click(object sender, EventArgs e)
        {
            BuildQueue();
        }

        private void mnuQueueProcess_Click(object sender, EventArgs e)
        {
            ProcessQueue();
        }

        private void mnuQueueStopAll_Click(object sender, EventArgs e)
        {
            queue.StopAll();
        }

        private void mnuQueueRemoveRows_Click(object sender, EventArgs e)
        {
            if (!IsBusy())
            {
                foreach (DataGridViewRow r in dgvQueue.SelectedRows)
                {
                    if (!r.IsNewRow)
                    {
                        DataRowView drv = (DataRowView)r.DataBoundItem;
                        string pk = (string)drv["PK"];
                        queue.Remove(pk);
                    }
                }
                RemoveSelectedRows(dgvQueue);
            }
        }

        private void mnuQueueMoveToHistory_Click(object sender, EventArgs e)
        {
            int cnt = 0;
            if (!IsBusy())
            {
                foreach (DataGridViewRow r in dgvQueue.SelectedRows)
                {
                    if (!r.IsNewRow)
                    {
                        DataRowView drv = (DataRowView)r.DataBoundItem;
                        string pk = (string)drv["PK"];
                        Media m = queue[pk];
                        m.Status = Media.StatusEnum.Downloaded;
                        if (!history.Contains(pk))
                        {
                            history.Add(m);
                            cnt++;
                        }
                        queue.Remove(pk);
                    }
                }
                history.Save(dsHistory.Tables[0]);
                RemoveSelectedRows(dgvQueue);
                wl("Rows moved to history: " + cnt.ToString());
            }
        }

        #endregion Queue

        #region History
        private void RefreshHistory()
        {
            if (isLowMemMode)
                return;
            try
            {
                history = (History)RcSerial.LoadFromFileFast(history.GetType(), history.XMLFullName);
                history.Sort();
            }
            catch (System.Exception ex)
            {
                wl(ex.Message);
            }
            history.ExportDataTable(dsHistory.Tables[0]);
        }

        private void mnuHistoryPurge_Click(object sender, EventArgs e)
        {
            if (IsBusy())
                return;
            DialogResult ret = MessageBox.Show("Are you Sure?", "Purge History", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ret == DialogResult.Yes)
            {
                history.Backup();
                history.Purge();
                RefreshHistory();
            }
        }

        private void mnuHistoryRemoveRows_Click(object sender, EventArgs e)
        {
            if (IsBusy())
                return;
            RemoveSelectedRows(dgvHistory);
        }

        private void mnuHistoryKeep_Click(object sender, EventArgs e)
        {
            int cnt = 0;
            foreach (DataGridViewRow r in dgvHistory.SelectedRows)
            {
                if (!r.IsNewRow)
                {
                    DataRowView drv = (DataRowView)r.DataBoundItem;
                    drv["DoSave"] = true;
                    cnt++;
                }
            }
            dgvHistory.Refresh();
            wl("Rows marked as keep: " + cnt.ToString());
        }

        private void mnuHistoryNoKeep_Click(object sender, EventArgs e)
        {
            int cnt = 0;
            foreach (DataGridViewRow r in dgvHistory.SelectedRows)
            {
                if (!r.IsNewRow)
                {
                    DataRowView drv = (DataRowView)r.DataBoundItem;
                    drv["DoSave"] = false;
                    cnt++;
                }
            }
            dgvHistory.Refresh();
            wl("Rows marked as do not keep: " + cnt.ToString());
        }

        private void mnuHistoryCheckSync_Click(object sender, EventArgs e)
        {
            int cnt = 0;
            foreach (DataGridViewRow r in dgvHistory.SelectedRows)
            {
                if (!r.IsNewRow)
                {
                    DataRowView drv = (DataRowView)r.DataBoundItem;
                    drv["Synced"] = true;
                    cnt++;
                }
            }
            dgvHistory.Refresh();
            wl("Rows marked as synced: " + cnt.ToString());
        }

        private void mnuHistoryUncheckSync_Click(object sender, EventArgs e)
        {
            int cnt = 0;
            foreach (DataGridViewRow r in dgvHistory.SelectedRows)
            {
                if (!r.IsNewRow)
                {
                    DataRowView drv = (DataRowView)r.DataBoundItem;
                    drv["Synced"] = false;
                    cnt++;
                }
            }
            dgvHistory.Refresh();
            wl("Rows marked as not synced: " + cnt.ToString());
        }
        #endregion History

        #region Log
        private void RefreshLog()
        {
            txtLog.Text = RcShared.ReadLog();
            txtLog.SelectionStart = txtLog.Text.Length;
            txtLog.ScrollToCaret();
            wl("Log refreshed.");
        }

        private void RefreshErrorLog()
        {
            txtErrorLog.Text = RcShared.ReadLog(RcShared.GetExeBaseName() + "-errors.log");
            txtErrorLog.SelectionStart = txtErrorLog.Text.Length;
            txtErrorLog.ScrollToCaret();
            wl("Error Log refreshed.");
        }

        private void mnuLogRefresh_Click(object sender, EventArgs e)
        {
            RefreshLog();
        }

        private void mnuErrorLogRefresh_Click(object sender, EventArgs e)
        {
            RefreshErrorLog();
        }
        #endregion Log

        #region Scheduler
        
        private void tsbStartSched_Click(object sender, EventArgs e)
        {
            StartSched();
        }

        private void tsbStopSched_Click(object sender, EventArgs e)
        {
            StopSched();
        }

        private void StartSched()
        {
            wl("Scheduler started.");
            notifyIcon1.Text = "rcFeedMe - Running";
            this.Text = APPVER + " - Running";            
            timerSched.Start();
            if (nextRun > System.DateTime.Now)
            {
                SetTitle();
            }
        }

        private void StopSched()
        {
            wl("Scheduler stopped.");
            notifyIcon1.Text = "rcFeedMe - Stopped";
            this.Text = APPVER + " - Stopped";
            timerSched.Stop();
        }

        private void timerSched_Tick(object sender, EventArgs e)
        {
            if (System.DateTime.Now > nextRun)
            {
                wl("Starting automatic scheduled run.");
                RunBatch();
                lastRun = System.DateTime.Now;
                nextRun = lastRun.AddMinutes(cfg.Freq);
                RcShared.WriteLog("Next scheduled run: " + nextRun.ToString());
                SetTitle();

            }
        }

        #endregion Scheduler

        #region Run Batch
        public void RunBatch()
        {
            try
            {
                RcShared.TrimLog(RcShared.GetLogName(), 9000, 10000);
                RcShared.TrimLog(RcShared.GetExeBaseName() + "-errors.log");
                if (IsBusy() || isBatchMode)
                {
                    wl("Aborting RunBatch since it's already still running.");
                    return;
                }
                RcShared.WriteLog("===================================================================");
                RcShared.WriteLog(APPVER + ": Starting RunBatch");
                RcShared.WriteLog("Lite mode: " + isLowMemMode.ToString());
                isBatchMode = true;
                RefreshAll();
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
        }

        private void RefreshAll()
        {
            try
            {
                if (IsBusy())
                {
                    return;
                }
                tabMain.SelectTab(tpFeeds);
                dgvFeeds.CurrentCell = null;
                SaveSettings();
                LoadSettings();
                progBar.Value = 0;
                progBar.Maximum = feeds.Count;
                wl("Refreshing all feeds....");
                SetStatus(StatusEnum.Feed);
                feeds.DumpRawXml = mnuDumpXml.Checked;
                if (!feeds.RefreshAll())
                {
                    wl("RefreshAll was aborted");
                }
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
        }    

        void feeds_ProgressChanged(object sender, EventArgs e)
        {
            try            
            {
                if (feeds.FeedsRefreshed <= progBar.Maximum)
                    progBar.Value = feeds.FeedsRefreshed;
                wl("Refreshing feed: " + progBar.Value + @"/" + progBar.Maximum);
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
        }       

        void feeds_FinishedRefresh(object sender, EventArgs e)
        {
            try
            {
                //DataGridViewRow dgvr = dgvFeeds.CurrentRow;
                DataTable dt = dsFeed.Tables[0];
                feeds.ExportMediaCollectionDataTable(dt);            
                SetStatus(StatusEnum.Idle);
                progBar.Value = 0;
                if (dgvFeeds.Rows.Count > 0)
                {
                    //if (dgvr == null)
                    dgvFeeds.CurrentCell = dgvFeeds.Rows[feedsRow].Cells[0];
                    //else
                    //    dgvFeeds.CurrentCell = dgvr.Cells[0];
                    FeedsRowChanged();
                }
                wl(toolStripStatusLabel1.Text + ".  Finished refreshing all feeds: " + feeds.FeedsRefreshed.ToString());
                if (isBatchMode)
                    BuildQueue();
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
        }

        private int BuildQueue()
        {
            if (IsBusy())
            {
                return 0;
            }
            //queue.Clear();
            try
            {
                wl("Build queue started....");
                int ret = queue.Build(feeds, groups, history, cfg, dsFeed.Tables[0]);
                queue.ExportDataTable(dsQueue.Tables[0]);
                wl("Build queue finished: " + ret.ToString());
                if (isBatchMode)
                    ProcessQueue();
                return ret;
            }
            catch (System.Exception ex)
            {
                FinalizeRunBatch();
                wl("ERROR: " + ex.Message);
                return 0;
            }
        }

        public void ProcessQueue()
        {
            try
            {
                if (IsBusy())
                {
                    return;
                }
                SetStatus(StatusEnum.Queue);
                wl("Processing Queue....");
                progBar.Value = 0;
                progBar.Maximum = queue.Count;
                tabMain.SelectTab(tpQueue);
                //queue.ProgressChanged += new EventHandler(queue_ProgressChanged);
                //queue.FinishedDownload += new EventHandler(queue_FinishedDownload);
                //queue.FinishedFile += new EventHandler(queue_FinishedFile);
                if (!queue.Process(feeds, history, errors, groups, cfg, dsQueue.Tables[0], dsHistory.Tables[0]))
                {                                        
                    SetStatus(StatusEnum.Idle);
                    FinalizeRunBatch();
                    wl("Nothing in the queue to process.");
                }
            }
            catch (System.Exception ex)
            {
                FinalizeRunBatch();
                wl("ERROR: " + ex.Message);
            }
        }

        void queue_ProgressChanged(object sender, EventArgs e)
        {
        }

        void queue_FinishedFile(object sender, EventArgs e)
        {
            try
            {
                if (queue.DLCount <= progBar.Maximum)
                {
                    progBar.Value = queue.DLCount;
                    wl("Processing Queue.  Files downloaded: " + queue.DLCount.ToString() + @"/" + progBar.Maximum.ToString());
                }
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
        }

        void queue_FinishedDownload(object sender, EventArgs e)
        {
            try
            {
                SetStatus(StatusEnum.Idle);
                wl("Queue has finished downloading.");
                FinalizeRunBatch();
                progBar.Value = 0;
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
        }

        private void FinalizeRunBatch()
        {
            try
            {
                CreatePlaylists();

                if (isLowMemMode)
                {
                    RcShared.WriteLog("Clear up memory for lite mode.");
                    RcShared.WriteLog("Memory used before GC: " + GC.GetTotalMemory(false).ToString("N0") + " bytes");
                    dsErrors.Clear();
                    dsHistory.Clear();
                    dsFeed.Clear();
                    dsFeeds.Clear();
                    dsQueue.Clear();
                    dsGroups.Clear();

                    history.Clear();
                    groups.Clear();
                    queue.Clear();
                    queue.CleanupTempVars();
                    errors.Clear();
                    feeds.Clear();

                    GC.Collect();
                    RcShared.WriteLog("Memory used after GC: " + GC.GetTotalMemory(true).ToString("N0") + " bytes");
                }

                if (isBatchMode)
                {
                    RcShared.WriteLog("Finished RunBatch");
                    RcShared.WriteLog("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                }
                

                SyncCount();

                if (autoStop)
                {                    
                    timerDelayStop.Start();                                                      
                }
            }
            catch (System.Exception ex)
            {
                wl("ERROR: " + ex.Message);
            }
            finally
            {
                isBatchMode = false;
            }
        }

        #endregion                               

        private void mnuGeneratePlaylists_Click(object sender, EventArgs e)
        {
            CreatePlaylists();
        }

        private void timerDelayStop_Tick(object sender, EventArgs e)
        {
            timerDelayStop.Stop();            
            formIsClosing();
            this.Close();            
            Application.Exit();
        }

       
            
    }//end class
}//end namespace
