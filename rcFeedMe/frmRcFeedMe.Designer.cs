﻿/*
Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
namespace rcFeedMe
{
    partial class frmRcFeedMe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRcFeedMe));
            this.mnuStrip = new System.Windows.Forms.MenuStrip();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuImportOpml = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExportOpml = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuGeneratePlaylists = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSaveSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLoadSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDumpXml = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDebugSyncCount = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.mnuTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuTrayStart = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTrayStop = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuRestore = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuTrayExit = new System.Windows.Forms.ToolStripMenuItem();
            this.timerSched = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbStartSched = new System.Windows.Forms.ToolStripButton();
            this.tsbStopSched = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbSaveReloadSettings = new System.Windows.Forms.ToolStripButton();
            this.tsbRefreshAll = new System.Windows.Forms.ToolStripButton();
            this.mnuRunBatch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbSearchHistory = new System.Windows.Forms.ToolStripButton();
            this.txtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSync = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsStatus2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.progBar = new System.Windows.Forms.ToolStripProgressBar();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tpFeeds = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvFeeds = new gfoidl.Windows.Forms.gfDataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.urlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dsGroupDD = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.keepCountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.includeExtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.includeTextDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SavePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrependDate = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PrependTitle = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SyncPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DoSync = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DoCacheFix = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Priority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DoDecode = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PrependShortDate = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PostDLExe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PostDLArgs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreSyncExe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreSyncArgs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuppressSyncCopy = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mnuFeeds = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuFeedsRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFeedsRefreshSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFeedsStopRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.dsFeeds = new System.Data.DataSet();
            this.dtFeeds = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn51 = new System.Data.DataColumn();
            this.dataColumn52 = new System.Data.DataColumn();
            this.dataColumn56 = new System.Data.DataColumn();
            this.dataColumn57 = new System.Data.DataColumn();
            this.dataColumn58 = new System.Data.DataColumn();
            this.dataColumn59 = new System.Data.DataColumn();
            this.dataColumn61 = new System.Data.DataColumn();
            this.dataColumn63 = new System.Data.DataColumn();
            this.dataColumn64 = new System.Data.DataColumn();
            this.dataColumn65 = new System.Data.DataColumn();
            this.dataColumn66 = new System.Data.DataColumn();
            this.dataColumn67 = new System.Data.DataColumn();
            this.dataColumn68 = new System.Data.DataColumn();
            this.dataColumn69 = new System.Data.DataColumn();
            this.dgvFeed = new gfoidl.Windows.Forms.gfDataGridView();
            this.btnAddQueue = new System.Windows.Forms.DataGridViewButtonColumn();
            this.InQueue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.summaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtPub = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mnuFeed = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuFeedAddSelected = new System.Windows.Forms.ToolStripMenuItem();
            this.dsFeed = new System.Data.DataSet();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn50 = new System.Data.DataColumn();
            this.tpQueue = new System.Windows.Forms.TabPage();
            this.dgvQueue = new gfoidl.Windows.Forms.gfDataGridView();
            this.nameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fileNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtPubDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Bytes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Attempt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Progress = new Sample.DataGridViewProgressColumn();
            this.mnuQueue = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuQueueBuild = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQueueProcess = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQueueStopAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQueueRemoveRows = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuQueueMoveToHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.dsQueue = new System.Data.DataSet();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.tpHistory = new System.Windows.Forms.TabPage();
            this.dgvHistory = new gfoidl.Windows.Forms.gfDataGridView();
            this.nameDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.summaryDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pubDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doSaveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.totalBytesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Synced = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mnuHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuHistoryPurge = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHistoryRemoveRows = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHistoryKeep = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHistoryNoKeep = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHistoryCheckSync = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHistoryUncheckSync = new System.Windows.Forms.ToolStripMenuItem();
            this.dsHistory = new System.Data.DataSet();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn30 = new System.Data.DataColumn();
            this.dataColumn31 = new System.Data.DataColumn();
            this.dataColumn32 = new System.Data.DataColumn();
            this.dataColumn33 = new System.Data.DataColumn();
            this.dataColumn34 = new System.Data.DataColumn();
            this.dataColumn35 = new System.Data.DataColumn();
            this.dataColumn36 = new System.Data.DataColumn();
            this.dataColumn37 = new System.Data.DataColumn();
            this.dataColumn38 = new System.Data.DataColumn();
            this.dataColumn48 = new System.Data.DataColumn();
            this.dataColumn54 = new System.Data.DataColumn();
            this.tpErrors = new System.Windows.Forms.TabPage();
            this.dgvErrors = new gfoidl.Windows.Forms.gfDataGridView();
            this.nameDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.summaryDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linkDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pubDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsErrors = new System.Data.DataSet();
            this.dataTable5 = new System.Data.DataTable();
            this.dataColumn39 = new System.Data.DataColumn();
            this.dataColumn40 = new System.Data.DataColumn();
            this.dataColumn41 = new System.Data.DataColumn();
            this.dataColumn42 = new System.Data.DataColumn();
            this.dataColumn43 = new System.Data.DataColumn();
            this.dataColumn44 = new System.Data.DataColumn();
            this.dataColumn45 = new System.Data.DataColumn();
            this.dataColumn46 = new System.Data.DataColumn();
            this.dataColumn47 = new System.Data.DataColumn();
            this.dataColumn49 = new System.Data.DataColumn();
            this.dataColumn55 = new System.Data.DataColumn();
            this.tpGroups = new System.Windows.Forms.TabPage();
            this.dgvGroups = new gfoidl.Windows.Forms.gfDataGridView();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.savePathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlaylistPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlaylistCutoffDays = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BareSyncPath = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PreSyncExeGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreSyncExeArgsGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoSyncCopy = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dsGroups = new System.Data.DataSet();
            this.dtGroups = new System.Data.DataTable();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn53 = new System.Data.DataColumn();
            this.dataColumn60 = new System.Data.DataColumn();
            this.dataColumn62 = new System.Data.DataColumn();
            this.dataColumn70 = new System.Data.DataColumn();
            this.dataColumn71 = new System.Data.DataColumn();
            this.dataColumn72 = new System.Data.DataColumn();
            this.dataColumn73 = new System.Data.DataColumn();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.chkDupErrors = new System.Windows.Forms.CheckBox();
            this.lblSupressDupError = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFreq = new System.Windows.Forms.Label();
            this.txtFreq = new System.Windows.Forms.TextBox();
            this.lblDefPath = new System.Windows.Forms.Label();
            this.lblDefKeepCount = new System.Windows.Forms.Label();
            this.txtDefPath = new System.Windows.Forms.TextBox();
            this.txtDefKeepCount = new System.Windows.Forms.TextBox();
            this.lblThreadScan = new System.Windows.Forms.Label();
            this.lblThreadDL = new System.Windows.Forms.Label();
            this.txtThreadScan = new System.Windows.Forms.TextBox();
            this.txtThreadDL = new System.Windows.Forms.TextBox();
            this.lblScanStall = new System.Windows.Forms.Label();
            this.lblDLStall = new System.Windows.Forms.Label();
            this.lblDLRetry = new System.Windows.Forms.Label();
            this.txtScanStall = new System.Windows.Forms.TextBox();
            this.txtDLStall = new System.Windows.Forms.TextBox();
            this.txtDLRetry = new System.Windows.Forms.TextBox();
            this.lblScanRetry = new System.Windows.Forms.Label();
            this.txtScanRetry = new System.Windows.Forms.TextBox();
            this.lblScanTimeout = new System.Windows.Forms.Label();
            this.lblDLTimeout = new System.Windows.Forms.Label();
            this.txtScanTimeout = new System.Windows.Forms.TextBox();
            this.txtDLTimeout = new System.Windows.Forms.TextBox();
            this.lblSyncPath = new System.Windows.Forms.Label();
            this.txtSyncPath = new System.Windows.Forms.TextBox();
            this.lblTimeZones = new System.Windows.Forms.Label();
            this.lblDateFormats = new System.Windows.Forms.Label();
            this.txtTimezones = new System.Windows.Forms.TextBox();
            this.txtDateFormats = new System.Windows.Forms.TextBox();
            this.tpLog = new System.Windows.Forms.TabPage();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.mnuLog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuLogRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.tpErrorLog = new System.Windows.Forms.TabPage();
            this.txtErrorLog = new System.Windows.Forms.TextBox();
            this.mnuErrorLog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuErrorLogRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timerDelayStart = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewProgressColumn1 = new Sample.DataGridViewProgressColumn();
            this.dataGridViewProgressColumn2 = new Sample.DataGridViewProgressColumn();
            this.timerDelayStop = new System.Windows.Forms.Timer(this.components);
            this.clearAllSyncFlagsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStrip.SuspendLayout();
            this.mnuTray.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tpFeeds.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGroupDD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.mnuFeeds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsFeeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFeeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeed)).BeginInit();
            this.mnuFeed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsFeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            this.tpQueue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueue)).BeginInit();
            this.mnuQueue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsQueue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            this.tpHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).BeginInit();
            this.mnuHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            this.tpErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).BeginInit();
            this.tpGroups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGroups)).BeginInit();
            this.tpSettings.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tpLog.SuspendLayout();
            this.mnuLog.SuspendLayout();
            this.tpErrorLog.SuspendLayout();
            this.mnuErrorLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuStrip
            // 
            this.mnuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.mnuDebug});
            this.mnuStrip.Location = new System.Drawing.Point(0, 0);
            this.mnuStrip.Name = "mnuStrip";
            this.mnuStrip.Size = new System.Drawing.Size(945, 24);
            this.mnuStrip.TabIndex = 0;
            this.mnuStrip.Text = "menuStrip1";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuImportOpml,
            this.mnuExportOpml,
            this.toolStripMenuItem2,
            this.mnuGeneratePlaylists,
            this.toolStripMenuItem5,
            this.mnuAbout,
            this.toolStripMenuItem1,
            this.mnuExit});
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.startToolStripMenuItem.Text = "&File";
            // 
            // mnuImportOpml
            // 
            this.mnuImportOpml.Name = "mnuImportOpml";
            this.mnuImportOpml.Size = new System.Drawing.Size(166, 22);
            this.mnuImportOpml.Text = "Import opml";
            this.mnuImportOpml.Click += new System.EventHandler(this.mnuImportOpml_Click);
            // 
            // mnuExportOpml
            // 
            this.mnuExportOpml.Name = "mnuExportOpml";
            this.mnuExportOpml.Size = new System.Drawing.Size(166, 22);
            this.mnuExportOpml.Text = "Export opml";
            this.mnuExportOpml.Click += new System.EventHandler(this.mnuExportOpml_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(163, 6);
            // 
            // mnuGeneratePlaylists
            // 
            this.mnuGeneratePlaylists.Name = "mnuGeneratePlaylists";
            this.mnuGeneratePlaylists.Size = new System.Drawing.Size(166, 22);
            this.mnuGeneratePlaylists.Text = "Generate Playlists";
            this.mnuGeneratePlaylists.Click += new System.EventHandler(this.mnuGeneratePlaylists_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(163, 6);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(166, 22);
            this.mnuAbout.Text = "About";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(163, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(166, 22);
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuDebug
            // 
            this.mnuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSaveSettings,
            this.mnuLoadSettings,
            this.mnuDumpXml,
            this.mnuDebugSyncCount,
            this.clearAllSyncFlagsToolStripMenuItem});
            this.mnuDebug.Name = "mnuDebug";
            this.mnuDebug.Size = new System.Drawing.Size(54, 20);
            this.mnuDebug.Text = "Debug";
            // 
            // mnuSaveSettings
            // 
            this.mnuSaveSettings.Name = "mnuSaveSettings";
            this.mnuSaveSettings.Size = new System.Drawing.Size(197, 22);
            this.mnuSaveSettings.Text = "Save Settings";
            this.mnuSaveSettings.Click += new System.EventHandler(this.mnuSaveSettings_Click);
            // 
            // mnuLoadSettings
            // 
            this.mnuLoadSettings.Name = "mnuLoadSettings";
            this.mnuLoadSettings.Size = new System.Drawing.Size(197, 22);
            this.mnuLoadSettings.Text = "Load Settings";
            this.mnuLoadSettings.Click += new System.EventHandler(this.mnuLoadSettings_Click);
            // 
            // mnuDumpXml
            // 
            this.mnuDumpXml.CheckOnClick = true;
            this.mnuDumpXml.Name = "mnuDumpXml";
            this.mnuDumpXml.Size = new System.Drawing.Size(197, 22);
            this.mnuDumpXml.Text = "Dump Raw XML Feeds?";
            // 
            // mnuDebugSyncCount
            // 
            this.mnuDebugSyncCount.Name = "mnuDebugSyncCount";
            this.mnuDebugSyncCount.Size = new System.Drawing.Size(197, 22);
            this.mnuDebugSyncCount.Text = "Sync Count";
            this.mnuDebugSyncCount.Click += new System.EventHandler(this.mnuDebugSyncCount_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.ContextMenuStrip = this.mnuTray;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "rcFeedMe";
            this.notifyIcon1.Visible = true;
            // 
            // mnuTray
            // 
            this.mnuTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTrayStart,
            this.mnuTrayStop,
            this.toolStripMenuItem3,
            this.mnuRestore,
            this.toolStripMenuItem4,
            this.mnuTrayExit});
            this.mnuTray.Name = "mnuTray";
            this.mnuTray.Size = new System.Drawing.Size(114, 104);
            // 
            // mnuTrayStart
            // 
            this.mnuTrayStart.Name = "mnuTrayStart";
            this.mnuTrayStart.Size = new System.Drawing.Size(113, 22);
            this.mnuTrayStart.Text = "Start";
            this.mnuTrayStart.Click += new System.EventHandler(this.mnuTrayStart_Click);
            // 
            // mnuTrayStop
            // 
            this.mnuTrayStop.Name = "mnuTrayStop";
            this.mnuTrayStop.Size = new System.Drawing.Size(113, 22);
            this.mnuTrayStop.Text = "Stop";
            this.mnuTrayStop.Click += new System.EventHandler(this.mnuTrayStop_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(110, 6);
            // 
            // mnuRestore
            // 
            this.mnuRestore.Name = "mnuRestore";
            this.mnuRestore.Size = new System.Drawing.Size(113, 22);
            this.mnuRestore.Text = "Restore";
            this.mnuRestore.Click += new System.EventHandler(this.mnuRestore_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(110, 6);
            // 
            // mnuTrayExit
            // 
            this.mnuTrayExit.Name = "mnuTrayExit";
            this.mnuTrayExit.Size = new System.Drawing.Size(113, 22);
            this.mnuTrayExit.Text = "Exit";
            this.mnuTrayExit.Click += new System.EventHandler(this.mnuTrayExit_Click);
            // 
            // timerSched
            // 
            this.timerSched.Interval = 5000;
            this.timerSched.Tick += new System.EventHandler(this.timerSched_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbStartSched,
            this.tsbStopSched,
            this.toolStripSeparator1,
            this.tsbSaveReloadSettings,
            this.tsbRefreshAll,
            this.mnuRunBatch,
            this.toolStripSeparator2,
            this.tsbSearchHistory,
            this.txtSearch,
            this.toolStripSeparator3,
            this.btnSync});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(945, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbStartSched
            // 
            this.tsbStartSched.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbStartSched.Image = ((System.Drawing.Image)(resources.GetObject("tsbStartSched.Image")));
            this.tsbStartSched.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStartSched.Name = "tsbStartSched";
            this.tsbStartSched.Size = new System.Drawing.Size(90, 22);
            this.tsbStartSched.Text = "Start Scheduler";
            this.tsbStartSched.ToolTipText = "Start Scheduler";
            this.tsbStartSched.Click += new System.EventHandler(this.tsbStartSched_Click);
            // 
            // tsbStopSched
            // 
            this.tsbStopSched.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbStopSched.Image = ((System.Drawing.Image)(resources.GetObject("tsbStopSched.Image")));
            this.tsbStopSched.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStopSched.Name = "tsbStopSched";
            this.tsbStopSched.Size = new System.Drawing.Size(90, 22);
            this.tsbStopSched.Text = "Stop Scheduler";
            this.tsbStopSched.ToolTipText = "Stop Scheduler";
            this.tsbStopSched.Click += new System.EventHandler(this.tsbStopSched_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbSaveReloadSettings
            // 
            this.tsbSaveReloadSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSaveReloadSettings.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveReloadSettings.Image")));
            this.tsbSaveReloadSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSaveReloadSettings.Name = "tsbSaveReloadSettings";
            this.tsbSaveReloadSettings.Size = new System.Drawing.Size(146, 22);
            this.tsbSaveReloadSettings.Text = "Save then Reload Settings";
            this.tsbSaveReloadSettings.Click += new System.EventHandler(this.tsbSaveReloadSettings_Click);
            // 
            // tsbRefreshAll
            // 
            this.tsbRefreshAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbRefreshAll.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefreshAll.Image")));
            this.tsbRefreshAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefreshAll.Name = "tsbRefreshAll";
            this.tsbRefreshAll.Size = new System.Drawing.Size(100, 22);
            this.tsbRefreshAll.Text = "Refresh All Feeds";
            this.tsbRefreshAll.ToolTipText = "Saves all Settings, reloads all settings, and refreshes all feeds.";
            this.tsbRefreshAll.Click += new System.EventHandler(this.tsbRefreshAll_Click);
            // 
            // mnuRunBatch
            // 
            this.mnuRunBatch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.mnuRunBatch.Image = ((System.Drawing.Image)(resources.GetObject("mnuRunBatch.Image")));
            this.mnuRunBatch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuRunBatch.Name = "mnuRunBatch";
            this.mnuRunBatch.Size = new System.Drawing.Size(101, 22);
            this.mnuRunBatch.Text = "Process All Feeds";
            this.mnuRunBatch.ToolTipText = "Does a Refresh All feeds, builds the queue, and processes the queue.  This is the" +
                " same thing that the scheduler runs.";
            this.mnuRunBatch.Click += new System.EventHandler(this.mnuRunBatch_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbSearchHistory
            // 
            this.tsbSearchHistory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbSearchHistory.Image = ((System.Drawing.Image)(resources.GetObject("tsbSearchHistory.Image")));
            this.tsbSearchHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSearchHistory.Name = "tsbSearchHistory";
            this.tsbSearchHistory.Size = new System.Drawing.Size(90, 22);
            this.tsbSearchHistory.Text = "Search History:";
            this.tsbSearchHistory.ToolTipText = "Search History: Substring search on Feed Name, title, and File Name.";
            this.tsbSearchHistory.Click += new System.EventHandler(this.tsbSearchHistory_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(76, 25);
            this.txtSearch.ToolTipText = "Search History: Substring search on Feed Name, title, and File Name.";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnSync
            // 
            this.btnSync.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSync.Image = ((System.Drawing.Image)(resources.GetObject("btnSync.Image")));
            this.btnSync.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSync.Name = "btnSync";
            this.btnSync.Size = new System.Drawing.Size(36, 22);
            this.btnSync.Text = "Sync";
            this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tsStatus2,
            this.progBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 652);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(945, 25);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabel1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(728, 20);
            this.toolStripStatusLabel1.Spring = true;
            this.toolStripStatusLabel1.Text = "Starting Up.  Please wait....";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsStatus2
            // 
            this.tsStatus2.AutoSize = false;
            this.tsStatus2.BackColor = System.Drawing.Color.LightGray;
            this.tsStatus2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.tsStatus2.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.tsStatus2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsStatus2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsStatus2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tsStatus2.Name = "tsStatus2";
            this.tsStatus2.Size = new System.Drawing.Size(100, 20);
            this.tsStatus2.Text = "Idle";
            // 
            // progBar
            // 
            this.progBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.progBar.Name = "progBar";
            this.progBar.Size = new System.Drawing.Size(100, 19);
            // 
            // tabMain
            // 
            this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMain.Controls.Add(this.tpFeeds);
            this.tabMain.Controls.Add(this.tpQueue);
            this.tabMain.Controls.Add(this.tpHistory);
            this.tabMain.Controls.Add(this.tpErrors);
            this.tabMain.Controls.Add(this.tpGroups);
            this.tabMain.Controls.Add(this.tpSettings);
            this.tabMain.Controls.Add(this.tpLog);
            this.tabMain.Controls.Add(this.tpErrorLog);
            this.tabMain.Location = new System.Drawing.Point(0, 51);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(947, 600);
            this.tabMain.TabIndex = 3;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tpFeeds
            // 
            this.tpFeeds.Controls.Add(this.splitContainer1);
            this.tpFeeds.Location = new System.Drawing.Point(4, 22);
            this.tpFeeds.Name = "tpFeeds";
            this.tpFeeds.Padding = new System.Windows.Forms.Padding(3);
            this.tpFeeds.Size = new System.Drawing.Size(939, 574);
            this.tpFeeds.TabIndex = 0;
            this.tpFeeds.Text = "Feeds";
            this.tpFeeds.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvFeeds);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvFeed);
            this.splitContainer1.Size = new System.Drawing.Size(933, 566);
            this.splitContainer1.SplitterDistance = 308;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvFeeds
            // 
            this.dgvFeeds.AllowUserToDeleteRows = false;
            this.dgvFeeds.AllowUserToOrderColumns = true;
            this.dgvFeeds.AutoGenerateColumns = false;
            this.dgvFeeds.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvFeeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFeeds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.urlDataGridViewTextBoxColumn,
            this.GroupName,
            this.keepCountDataGridViewTextBoxColumn,
            this.includeExtDataGridViewTextBoxColumn,
            this.includeTextDataGridViewTextBoxColumn,
            this.SavePath,
            this.PrependDate,
            this.PrependTitle,
            this.SyncPath,
            this.DoSync,
            this.DoCacheFix,
            this.Username,
            this.Password,
            this.Priority,
            this.DoDecode,
            this.PrependShortDate,
            this.PostDLExe,
            this.PostDLArgs,
            this.PreSyncExe,
            this.PreSyncArgs,
            this.SuppressSyncCopy});
            this.dgvFeeds.ContextMenuStrip = this.mnuFeeds;
            this.dgvFeeds.DataMember = "dtFeeds";
            this.dgvFeeds.DataSource = this.dsFeeds;
            this.dgvFeeds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFeeds.Location = new System.Drawing.Point(0, 0);
            this.dgvFeeds.Name = "dgvFeeds";
            this.dgvFeeds.RowTemplate.Height = 24;
            this.dgvFeeds.Size = new System.Drawing.Size(933, 308);
            this.dgvFeeds.TabIndex = 0;
            this.dgvFeeds.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFeeds_CellEndEdit);
            this.dgvFeeds.CurrentCellChanged += new System.EventHandler(this.dgvFeeds_CurrentCellChanged);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Width = 60;
            // 
            // urlDataGridViewTextBoxColumn
            // 
            this.urlDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.urlDataGridViewTextBoxColumn.DataPropertyName = "Url";
            this.urlDataGridViewTextBoxColumn.HeaderText = "Url";
            this.urlDataGridViewTextBoxColumn.MinimumWidth = 10;
            this.urlDataGridViewTextBoxColumn.Name = "urlDataGridViewTextBoxColumn";
            this.urlDataGridViewTextBoxColumn.Width = 300;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "GroupName";
            this.GroupName.DataSource = this.dsGroupDD;
            this.GroupName.DisplayMember = "Table1.decode";
            this.GroupName.HeaderText = "Group";
            this.GroupName.Name = "GroupName";
            this.GroupName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.GroupName.ValueMember = "Table1.code";
            this.GroupName.Width = 61;
            // 
            // dsGroupDD
            // 
            this.dsGroupDD.DataSetName = "NewDataSet";
            this.dsGroupDD.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11});
            this.dataTable1.TableName = "Table1";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "code";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "decode";
            // 
            // keepCountDataGridViewTextBoxColumn
            // 
            this.keepCountDataGridViewTextBoxColumn.DataPropertyName = "KeepCount";
            this.keepCountDataGridViewTextBoxColumn.HeaderText = "Keep Count";
            this.keepCountDataGridViewTextBoxColumn.Name = "keepCountDataGridViewTextBoxColumn";
            this.keepCountDataGridViewTextBoxColumn.ToolTipText = "How many files for the specific feed do you want to keep.  Also implies maximum n" +
                "umber of files to download.  Enter -1 to download and keep everything.";
            this.keepCountDataGridViewTextBoxColumn.Width = 81;
            // 
            // includeExtDataGridViewTextBoxColumn
            // 
            this.includeExtDataGridViewTextBoxColumn.DataPropertyName = "IncludeExt";
            this.includeExtDataGridViewTextBoxColumn.HeaderText = "Include Extension";
            this.includeExtDataGridViewTextBoxColumn.Name = "includeExtDataGridViewTextBoxColumn";
            this.includeExtDataGridViewTextBoxColumn.ToolTipText = "pipe(|) delimited list of file extensions(include period(.)) to match against for" +
                " downloads";
            this.includeExtDataGridViewTextBoxColumn.Width = 106;
            // 
            // includeTextDataGridViewTextBoxColumn
            // 
            this.includeTextDataGridViewTextBoxColumn.DataPropertyName = "IncludeText";
            this.includeTextDataGridViewTextBoxColumn.HeaderText = "Include Text";
            this.includeTextDataGridViewTextBoxColumn.Name = "includeTextDataGridViewTextBoxColumn";
            this.includeTextDataGridViewTextBoxColumn.ToolTipText = "pipe(|) delimited list of substrings that are compared to the filenames for downl" +
                "oads";
            this.includeTextDataGridViewTextBoxColumn.Width = 84;
            // 
            // SavePath
            // 
            this.SavePath.DataPropertyName = "SavePath";
            this.SavePath.HeaderText = "Save Path";
            this.SavePath.Name = "SavePath";
            this.SavePath.ToolTipText = "Enter the complete path where the podcasts for this feed will be saved.  This pat" +
                "h overrides all other paths (groups and global settings).";
            this.SavePath.Width = 76;
            // 
            // PrependDate
            // 
            this.PrependDate.DataPropertyName = "PrependDate";
            this.PrependDate.HeaderText = "Prepend Date?";
            this.PrependDate.Name = "PrependDate";
            this.PrependDate.ToolTipText = "Prepend the Publish Date to filename.  Useful to make the filenames unique and he" +
                "lps in sorting.  If both prepend date and prepend title are checked, prepend dat" +
                "e comes first.";
            this.PrependDate.Width = 77;
            // 
            // PrependTitle
            // 
            this.PrependTitle.DataPropertyName = "Prepend";
            this.PrependTitle.HeaderText = "Prepend Title?";
            this.PrependTitle.Name = "PrependTitle";
            this.PrependTitle.ToolTipText = "Prepend title to filename.  Useful to make the filenames unique if only a generic" +
                " filename is used in the feed.";
            this.PrependTitle.Width = 74;
            // 
            // SyncPath
            // 
            this.SyncPath.DataPropertyName = "SyncPath";
            this.SyncPath.HeaderText = "SyncPath";
            this.SyncPath.Name = "SyncPath";
            this.SyncPath.ToolTipText = "Enter the complete path where the podcasts for this feed will be synced to.  This" +
                " path overrides all other paths (groups and global settings).";
            this.SyncPath.Width = 78;
            // 
            // DoSync
            // 
            this.DoSync.DataPropertyName = "DoSync";
            this.DoSync.HeaderText = "Sync?";
            this.DoSync.MinimumWidth = 58;
            this.DoSync.Name = "DoSync";
            this.DoSync.ToolTipText = "Check this if you wish to sync the files from this feed to your mp3 player.";
            this.DoSync.Width = 58;
            // 
            // DoCacheFix
            // 
            this.DoCacheFix.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DoCacheFix.DataPropertyName = "DoCacheFix";
            this.DoCacheFix.HeaderText = "Apply Cache Fix?";
            this.DoCacheFix.MinimumWidth = 84;
            this.DoCacheFix.Name = "DoCacheFix";
            this.DoCacheFix.ToolTipText = "Appends a fake query string to the end of the url when making the request for the" +
                " feed.  This guarantees a cached copy will not be used but also causes some site" +
                "s to throw an error.";
            this.DoCacheFix.Width = 84;
            // 
            // Username
            // 
            this.Username.DataPropertyName = "Username";
            this.Username.HeaderText = "Username";
            this.Username.MaxInputLength = 50;
            this.Username.Name = "Username";
            this.Username.ToolTipText = "User name if site requres login";
            this.Username.Width = 80;
            // 
            // Password
            // 
            this.Password.DataPropertyName = "Password";
            this.Password.HeaderText = "Password";
            this.Password.MaxInputLength = 50;
            this.Password.Name = "Password";
            this.Password.ToolTipText = "Password if site requires login";
            this.Password.Width = 78;
            // 
            // Priority
            // 
            this.Priority.DataPropertyName = "Priority";
            this.Priority.HeaderText = "Priority";
            this.Priority.Name = "Priority";
            this.Priority.ToolTipText = "Priority for playlist generation.";
            this.Priority.Width = 63;
            // 
            // DoDecode
            // 
            this.DoDecode.DataPropertyName = "DoDecode";
            this.DoDecode.HeaderText = "Decode?";
            this.DoDecode.Name = "DoDecode";
            this.DoDecode.ToolTipText = "Apply Url Decode to filename?";
            this.DoDecode.Width = 57;
            // 
            // PrependShortDate
            // 
            this.PrependShortDate.DataPropertyName = "PrependShortDate";
            this.PrependShortDate.HeaderText = "Prepend Short Date?";
            this.PrependShortDate.Name = "PrependShortDate";
            this.PrependShortDate.Width = 76;
            // 
            // PostDLExe
            // 
            this.PostDLExe.DataPropertyName = "PostDLExe";
            this.PostDLExe.HeaderText = "Post DL Exe";
            this.PostDLExe.Name = "PostDLExe";
            this.PostDLExe.Width = 68;
            // 
            // PostDLArgs
            // 
            this.PostDLArgs.DataPropertyName = "PostDLArgs";
            this.PostDLArgs.HeaderText = "Post DL Args";
            this.PostDLArgs.Name = "PostDLArgs";
            this.PostDLArgs.Width = 87;
            // 
            // PreSyncExe
            // 
            this.PreSyncExe.DataPropertyName = "PreSyncExe";
            this.PreSyncExe.HeaderText = "Pre Sync Exe";
            this.PreSyncExe.Name = "PreSyncExe";
            this.PreSyncExe.Width = 88;
            // 
            // PreSyncArgs
            // 
            this.PreSyncArgs.DataPropertyName = "PreSyncArgs";
            this.PreSyncArgs.HeaderText = "Pre Sync Args";
            this.PreSyncArgs.Name = "PreSyncArgs";
            this.PreSyncArgs.Width = 91;
            // 
            // SuppressSyncCopy
            // 
            this.SuppressSyncCopy.DataPropertyName = "SuppressSyncCopy";
            this.SuppressSyncCopy.HeaderText = "Suppress Sync Copy";
            this.SuppressSyncCopy.Name = "SuppressSyncCopy";
            this.SuppressSyncCopy.Width = 78;
            // 
            // mnuFeeds
            // 
            this.mnuFeeds.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFeedsRemove,
            this.mnuFeedsRefreshSelected,
            this.mnuFeedsStopRefresh});
            this.mnuFeeds.Name = "mnuFeeds";
            this.mnuFeeds.Size = new System.Drawing.Size(196, 70);
            // 
            // mnuFeedsRemove
            // 
            this.mnuFeedsRemove.Name = "mnuFeedsRemove";
            this.mnuFeedsRemove.Size = new System.Drawing.Size(195, 22);
            this.mnuFeedsRemove.Text = "Remove Selected Rows";
            this.mnuFeedsRemove.Click += new System.EventHandler(this.mnuFeedsRemove_Click);
            // 
            // mnuFeedsRefreshSelected
            // 
            this.mnuFeedsRefreshSelected.Name = "mnuFeedsRefreshSelected";
            this.mnuFeedsRefreshSelected.Size = new System.Drawing.Size(195, 22);
            this.mnuFeedsRefreshSelected.Text = "Refresh Selected Rows";
            this.mnuFeedsRefreshSelected.Click += new System.EventHandler(this.mnuFeedsRefreshSelected_Click);
            // 
            // mnuFeedsStopRefresh
            // 
            this.mnuFeedsStopRefresh.Name = "mnuFeedsStopRefresh";
            this.mnuFeedsStopRefresh.Size = new System.Drawing.Size(195, 22);
            this.mnuFeedsStopRefresh.Text = "Stop Refresh";
            this.mnuFeedsStopRefresh.Click += new System.EventHandler(this.mnuFeedsStopRefresh_Click);
            // 
            // dsFeeds
            // 
            this.dsFeeds.DataSetName = "dsFeeds";
            this.dsFeeds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtFeeds});
            // 
            // dtFeeds
            // 
            this.dtFeeds.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn19,
            this.dataColumn51,
            this.dataColumn52,
            this.dataColumn56,
            this.dataColumn57,
            this.dataColumn58,
            this.dataColumn59,
            this.dataColumn61,
            this.dataColumn63,
            this.dataColumn64,
            this.dataColumn65,
            this.dataColumn66,
            this.dataColumn67,
            this.dataColumn68,
            this.dataColumn69});
            this.dtFeeds.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Name"}, true)});
            this.dtFeeds.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.dtFeeds.TableName = "dtFeeds";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "Name";
            this.dataColumn1.DefaultValue = "";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Url";
            this.dataColumn2.DefaultValue = "";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "GroupName";
            this.dataColumn3.DefaultValue = "";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "KeepCount";
            this.dataColumn4.DataType = typeof(int);
            this.dataColumn4.DefaultValue = 5;
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "IncludeExt";
            this.dataColumn5.DefaultValue = "";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "IncludeText";
            this.dataColumn6.DefaultValue = "";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "SavePath";
            this.dataColumn7.DefaultValue = "";
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "Prepend";
            this.dataColumn19.DataType = typeof(bool);
            this.dataColumn19.DefaultValue = false;
            // 
            // dataColumn51
            // 
            this.dataColumn51.ColumnName = "SyncPath";
            // 
            // dataColumn52
            // 
            this.dataColumn52.ColumnName = "DoSync";
            this.dataColumn52.DataType = typeof(bool);
            this.dataColumn52.DefaultValue = false;
            // 
            // dataColumn56
            // 
            this.dataColumn56.ColumnName = "PrependDate";
            this.dataColumn56.DataType = typeof(bool);
            this.dataColumn56.DefaultValue = false;
            // 
            // dataColumn57
            // 
            this.dataColumn57.ColumnName = "DoCacheFix";
            this.dataColumn57.DataType = typeof(bool);
            this.dataColumn57.DefaultValue = false;
            // 
            // dataColumn58
            // 
            this.dataColumn58.ColumnName = "Username";
            // 
            // dataColumn59
            // 
            this.dataColumn59.ColumnName = "Password";
            // 
            // dataColumn61
            // 
            this.dataColumn61.ColumnName = "Priority";
            this.dataColumn61.DataType = typeof(int);
            // 
            // dataColumn63
            // 
            this.dataColumn63.ColumnName = "DoDecode";
            this.dataColumn63.DataType = typeof(bool);
            this.dataColumn63.DefaultValue = false;
            // 
            // dataColumn64
            // 
            this.dataColumn64.ColumnName = "PrependShortDate";
            this.dataColumn64.DataType = typeof(bool);
            this.dataColumn64.DefaultValue = false;
            // 
            // dataColumn65
            // 
            this.dataColumn65.ColumnName = "PostDLExe";
            this.dataColumn65.DefaultValue = "";
            // 
            // dataColumn66
            // 
            this.dataColumn66.ColumnName = "PostDLArgs";
            this.dataColumn66.DefaultValue = "";
            // 
            // dataColumn67
            // 
            this.dataColumn67.ColumnName = "PreSyncExe";
            this.dataColumn67.DefaultValue = "";
            // 
            // dataColumn68
            // 
            this.dataColumn68.ColumnName = "PreSyncArgs";
            this.dataColumn68.DefaultValue = "";
            // 
            // dataColumn69
            // 
            this.dataColumn69.ColumnName = "SuppressSyncCopy";
            this.dataColumn69.DataType = typeof(bool);
            this.dataColumn69.DefaultValue = false;
            // 
            // dgvFeed
            // 
            this.dgvFeed.AllowUserToAddRows = false;
            this.dgvFeed.AllowUserToDeleteRows = false;
            this.dgvFeed.AllowUserToOrderColumns = true;
            this.dgvFeed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFeed.AutoGenerateColumns = false;
            this.dgvFeed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvFeed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFeed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnAddQueue,
            this.InQueue,
            this.titleDataGridViewTextBoxColumn,
            this.summaryDataGridViewTextBoxColumn,
            this.dtPub,
            this.FileName,
            this.linkDataGridViewTextBoxColumn});
            this.dgvFeed.ContextMenuStrip = this.mnuFeed;
            this.dgvFeed.DataMember = "dtFeed";
            this.dgvFeed.DataSource = this.dsFeed;
            this.dgvFeed.Location = new System.Drawing.Point(0, 2);
            this.dgvFeed.Name = "dgvFeed";
            this.dgvFeed.RowTemplate.Height = 24;
            this.dgvFeed.Size = new System.Drawing.Size(933, 252);
            this.dgvFeed.TabIndex = 0;
            this.dgvFeed.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFeed_CellContentClick);
            // 
            // btnAddQueue
            // 
            this.btnAddQueue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.btnAddQueue.HeaderText = "";
            this.btnAddQueue.Name = "btnAddQueue";
            this.btnAddQueue.Text = "Add to Queue";
            this.btnAddQueue.UseColumnTextForButtonValue = true;
            this.btnAddQueue.Width = 5;
            // 
            // InQueue
            // 
            this.InQueue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.InQueue.DataPropertyName = "InQueue";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.NullValue = false;
            this.InQueue.DefaultCellStyle = dataGridViewCellStyle1;
            this.InQueue.FillWeight = 10F;
            this.InQueue.HeaderText = "In Queue?";
            this.InQueue.Name = "InQueue";
            this.InQueue.ReadOnly = true;
            this.InQueue.ToolTipText = "Is the file already in the download queue?";
            this.InQueue.Width = 63;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.MinimumWidth = 10;
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.Width = 80;
            // 
            // summaryDataGridViewTextBoxColumn
            // 
            this.summaryDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.summaryDataGridViewTextBoxColumn.DataPropertyName = "Summary";
            this.summaryDataGridViewTextBoxColumn.HeaderText = "Summary";
            this.summaryDataGridViewTextBoxColumn.MinimumWidth = 10;
            this.summaryDataGridViewTextBoxColumn.Name = "summaryDataGridViewTextBoxColumn";
            this.summaryDataGridViewTextBoxColumn.Width = 250;
            // 
            // dtPub
            // 
            this.dtPub.DataPropertyName = "dtPub";
            this.dtPub.HeaderText = "Publish Date";
            this.dtPub.Name = "dtPub";
            this.dtPub.Width = 92;
            // 
            // FileName
            // 
            this.FileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.FileName.DataPropertyName = "FileName";
            this.FileName.HeaderText = "File Name";
            this.FileName.Name = "FileName";
            this.FileName.Width = 79;
            // 
            // linkDataGridViewTextBoxColumn
            // 
            this.linkDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.linkDataGridViewTextBoxColumn.DataPropertyName = "Link";
            this.linkDataGridViewTextBoxColumn.HeaderText = "Link";
            this.linkDataGridViewTextBoxColumn.MinimumWidth = 10;
            this.linkDataGridViewTextBoxColumn.Name = "linkDataGridViewTextBoxColumn";
            this.linkDataGridViewTextBoxColumn.Width = 52;
            // 
            // mnuFeed
            // 
            this.mnuFeed.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFeedAddSelected});
            this.mnuFeed.Name = "mnuFeed";
            this.mnuFeed.Size = new System.Drawing.Size(221, 26);
            // 
            // mnuFeedAddSelected
            // 
            this.mnuFeedAddSelected.Name = "mnuFeedAddSelected";
            this.mnuFeedAddSelected.Size = new System.Drawing.Size(220, 22);
            this.mnuFeedAddSelected.Text = "Add selected rows to queue";
            this.mnuFeedAddSelected.Click += new System.EventHandler(this.mnuFeedAddSelected_Click);
            // 
            // dsFeed
            // 
            this.dsFeed.DataSetName = "NewDataSet";
            this.dsFeed.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable2});
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18,
            this.dataColumn50});
            this.dataTable2.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "PK"}, true)});
            this.dataTable2.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn50};
            this.dataTable2.TableName = "dtFeed";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "Title";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "Summary";
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "Link";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "InQueue";
            this.dataColumn15.DataType = typeof(bool);
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "FileName";
            // 
            // dataColumn17
            // 
            this.dataColumn17.ColumnName = "dtPub";
            this.dataColumn17.DataType = typeof(System.DateTime);
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "Name";
            // 
            // dataColumn50
            // 
            this.dataColumn50.AllowDBNull = false;
            this.dataColumn50.ColumnName = "PK";
            // 
            // tpQueue
            // 
            this.tpQueue.Controls.Add(this.dgvQueue);
            this.tpQueue.Location = new System.Drawing.Point(4, 22);
            this.tpQueue.Name = "tpQueue";
            this.tpQueue.Size = new System.Drawing.Size(939, 574);
            this.tpQueue.TabIndex = 4;
            this.tpQueue.Text = "Download Queue";
            this.tpQueue.UseVisualStyleBackColor = true;
            // 
            // dgvQueue
            // 
            this.dgvQueue.AllowUserToAddRows = false;
            this.dgvQueue.AllowUserToDeleteRows = false;
            this.dgvQueue.AllowUserToOrderColumns = true;
            this.dgvQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvQueue.AutoGenerateColumns = false;
            this.dgvQueue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQueue.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn2,
            this.titleDataGridViewTextBoxColumn1,
            this.linkDataGridViewTextBoxColumn1,
            this.fileNameDataGridViewTextBoxColumn,
            this.dtPubDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.Bytes,
            this.Attempt,
            this.Progress});
            this.dgvQueue.ContextMenuStrip = this.mnuQueue;
            this.dgvQueue.DataMember = "Table1";
            this.dgvQueue.DataSource = this.dsQueue;
            this.dgvQueue.Location = new System.Drawing.Point(3, 3);
            this.dgvQueue.Name = "dgvQueue";
            this.dgvQueue.ReadOnly = true;
            this.dgvQueue.RowTemplate.Height = 24;
            this.dgvQueue.Size = new System.Drawing.Size(933, 554);
            this.dgvQueue.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn2
            // 
            this.nameDataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.nameDataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn2.HeaderText = "Feed Name";
            this.nameDataGridViewTextBoxColumn2.Name = "nameDataGridViewTextBoxColumn2";
            this.nameDataGridViewTextBoxColumn2.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn2.Width = 87;
            // 
            // titleDataGridViewTextBoxColumn1
            // 
            this.titleDataGridViewTextBoxColumn1.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn1.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn1.MinimumWidth = 150;
            this.titleDataGridViewTextBoxColumn1.Name = "titleDataGridViewTextBoxColumn1";
            this.titleDataGridViewTextBoxColumn1.ReadOnly = true;
            this.titleDataGridViewTextBoxColumn1.Width = 300;
            // 
            // linkDataGridViewTextBoxColumn1
            // 
            this.linkDataGridViewTextBoxColumn1.DataPropertyName = "Link";
            this.linkDataGridViewTextBoxColumn1.HeaderText = "Link";
            this.linkDataGridViewTextBoxColumn1.Name = "linkDataGridViewTextBoxColumn1";
            this.linkDataGridViewTextBoxColumn1.ReadOnly = true;
            this.linkDataGridViewTextBoxColumn1.Width = 52;
            // 
            // fileNameDataGridViewTextBoxColumn
            // 
            this.fileNameDataGridViewTextBoxColumn.DataPropertyName = "FileName";
            this.fileNameDataGridViewTextBoxColumn.HeaderText = "File Name";
            this.fileNameDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.fileNameDataGridViewTextBoxColumn.Name = "fileNameDataGridViewTextBoxColumn";
            this.fileNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.fileNameDataGridViewTextBoxColumn.Width = 250;
            // 
            // dtPubDataGridViewTextBoxColumn
            // 
            this.dtPubDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dtPubDataGridViewTextBoxColumn.DataPropertyName = "dtPub";
            this.dtPubDataGridViewTextBoxColumn.HeaderText = "Publish Date";
            this.dtPubDataGridViewTextBoxColumn.Name = "dtPubDataGridViewTextBoxColumn";
            this.dtPubDataGridViewTextBoxColumn.ReadOnly = true;
            this.dtPubDataGridViewTextBoxColumn.Width = 92;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn.Width = 62;
            // 
            // Bytes
            // 
            this.Bytes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Bytes.DataPropertyName = "Bytes";
            this.Bytes.HeaderText = "Bytes";
            this.Bytes.Name = "Bytes";
            this.Bytes.ReadOnly = true;
            this.Bytes.Width = 58;
            // 
            // Attempt
            // 
            this.Attempt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Attempt.DataPropertyName = "Attempt";
            this.Attempt.HeaderText = "Attempt";
            this.Attempt.Name = "Attempt";
            this.Attempt.ReadOnly = true;
            this.Attempt.Width = 68;
            // 
            // Progress
            // 
            this.Progress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Progress.DataPropertyName = "Progress";
            this.Progress.HeaderText = "Progress";
            this.Progress.MinimumWidth = 100;
            this.Progress.Name = "Progress";
            this.Progress.ReadOnly = true;
            // 
            // mnuQueue
            // 
            this.mnuQueue.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuQueueBuild,
            this.mnuQueueProcess,
            this.mnuQueueStopAll,
            this.mnuQueueRemoveRows,
            this.mnuQueueMoveToHistory});
            this.mnuQueue.Name = "mnuQueue";
            this.mnuQueue.Size = new System.Drawing.Size(238, 114);
            // 
            // mnuQueueBuild
            // 
            this.mnuQueueBuild.Name = "mnuQueueBuild";
            this.mnuQueueBuild.Size = new System.Drawing.Size(237, 22);
            this.mnuQueueBuild.Text = "Build Queue";
            this.mnuQueueBuild.Click += new System.EventHandler(this.mnuQueueBuild_Click);
            // 
            // mnuQueueProcess
            // 
            this.mnuQueueProcess.Name = "mnuQueueProcess";
            this.mnuQueueProcess.Size = new System.Drawing.Size(237, 22);
            this.mnuQueueProcess.Text = "Process Queue";
            this.mnuQueueProcess.Click += new System.EventHandler(this.mnuQueueProcess_Click);
            // 
            // mnuQueueStopAll
            // 
            this.mnuQueueStopAll.Name = "mnuQueueStopAll";
            this.mnuQueueStopAll.Size = new System.Drawing.Size(237, 22);
            this.mnuQueueStopAll.Text = "Stop All Downloads";
            this.mnuQueueStopAll.Click += new System.EventHandler(this.mnuQueueStopAll_Click);
            // 
            // mnuQueueRemoveRows
            // 
            this.mnuQueueRemoveRows.Name = "mnuQueueRemoveRows";
            this.mnuQueueRemoveRows.Size = new System.Drawing.Size(237, 22);
            this.mnuQueueRemoveRows.Text = "Remove Selected Rows";
            this.mnuQueueRemoveRows.Click += new System.EventHandler(this.mnuQueueRemoveRows_Click);
            // 
            // mnuQueueMoveToHistory
            // 
            this.mnuQueueMoveToHistory.Name = "mnuQueueMoveToHistory";
            this.mnuQueueMoveToHistory.Size = new System.Drawing.Size(237, 22);
            this.mnuQueueMoveToHistory.Text = "Move Selected Rows to History";
            this.mnuQueueMoveToHistory.Click += new System.EventHandler(this.mnuQueueMoveToHistory_Click);
            // 
            // dsQueue
            // 
            this.dsQueue.DataSetName = "NewDataSet";
            this.dsQueue.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable3});
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25,
            this.dataColumn26,
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn29});
            this.dataTable3.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "PK"}, true)});
            this.dataTable3.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn28};
            this.dataTable3.TableName = "Table1";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "Name";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "Title";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "Link";
            // 
            // dataColumn23
            // 
            this.dataColumn23.ColumnName = "FileName";
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "dtPub";
            this.dataColumn24.DataType = typeof(System.DateTime);
            // 
            // dataColumn25
            // 
            this.dataColumn25.ColumnName = "Status";
            // 
            // dataColumn26
            // 
            this.dataColumn26.ColumnName = "Bytes";
            // 
            // dataColumn27
            // 
            this.dataColumn27.ColumnName = "Progress";
            this.dataColumn27.DataType = typeof(int);
            // 
            // dataColumn28
            // 
            this.dataColumn28.AllowDBNull = false;
            this.dataColumn28.ColumnName = "PK";
            // 
            // dataColumn29
            // 
            this.dataColumn29.ColumnName = "Attempt";
            this.dataColumn29.DataType = typeof(int);
            // 
            // tpHistory
            // 
            this.tpHistory.Controls.Add(this.dgvHistory);
            this.tpHistory.Location = new System.Drawing.Point(4, 22);
            this.tpHistory.Name = "tpHistory";
            this.tpHistory.Size = new System.Drawing.Size(939, 574);
            this.tpHistory.TabIndex = 5;
            this.tpHistory.Text = "Download History";
            this.tpHistory.ToolTipText = "History of all successfully downloaded files.";
            this.tpHistory.UseVisualStyleBackColor = true;
            // 
            // dgvHistory
            // 
            this.dgvHistory.AllowUserToAddRows = false;
            this.dgvHistory.AllowUserToDeleteRows = false;
            this.dgvHistory.AllowUserToOrderColumns = true;
            this.dgvHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvHistory.AutoGenerateColumns = false;
            this.dgvHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn3,
            this.titleDataGridViewTextBoxColumn2,
            this.summaryDataGridViewTextBoxColumn1,
            this.linkDataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn1,
            this.fullNameDataGridViewTextBoxColumn,
            this.pubDateDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn1,
            this.doSaveDataGridViewCheckBoxColumn,
            this.totalBytesDataGridViewTextBoxColumn,
            this.Synced});
            this.dgvHistory.ContextMenuStrip = this.mnuHistory;
            this.dgvHistory.DataMember = "Table1";
            this.dgvHistory.DataSource = this.dsHistory;
            this.dgvHistory.Location = new System.Drawing.Point(3, 3);
            this.dgvHistory.Name = "dgvHistory";
            this.dgvHistory.RowTemplate.Height = 24;
            this.dgvHistory.Size = new System.Drawing.Size(933, 557);
            this.dgvHistory.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn3
            // 
            this.nameDataGridViewTextBoxColumn3.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn3.HeaderText = "Feed Name";
            this.nameDataGridViewTextBoxColumn3.Name = "nameDataGridViewTextBoxColumn3";
            this.nameDataGridViewTextBoxColumn3.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn3.Width = 87;
            // 
            // titleDataGridViewTextBoxColumn2
            // 
            this.titleDataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.titleDataGridViewTextBoxColumn2.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn2.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn2.MinimumWidth = 10;
            this.titleDataGridViewTextBoxColumn2.Name = "titleDataGridViewTextBoxColumn2";
            this.titleDataGridViewTextBoxColumn2.Width = 150;
            // 
            // summaryDataGridViewTextBoxColumn1
            // 
            this.summaryDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.summaryDataGridViewTextBoxColumn1.DataPropertyName = "Summary";
            this.summaryDataGridViewTextBoxColumn1.HeaderText = "Summary";
            this.summaryDataGridViewTextBoxColumn1.MinimumWidth = 10;
            this.summaryDataGridViewTextBoxColumn1.Name = "summaryDataGridViewTextBoxColumn1";
            this.summaryDataGridViewTextBoxColumn1.ReadOnly = true;
            this.summaryDataGridViewTextBoxColumn1.Width = 75;
            // 
            // linkDataGridViewTextBoxColumn2
            // 
            this.linkDataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.linkDataGridViewTextBoxColumn2.DataPropertyName = "Link";
            this.linkDataGridViewTextBoxColumn2.HeaderText = "Link";
            this.linkDataGridViewTextBoxColumn2.MinimumWidth = 10;
            this.linkDataGridViewTextBoxColumn2.Name = "linkDataGridViewTextBoxColumn2";
            this.linkDataGridViewTextBoxColumn2.ReadOnly = true;
            this.linkDataGridViewTextBoxColumn2.Width = 52;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "FileName";
            this.dataGridViewTextBoxColumn1.HeaderText = "File Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 79;
            // 
            // fullNameDataGridViewTextBoxColumn
            // 
            this.fullNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.fullNameDataGridViewTextBoxColumn.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn.HeaderText = "Full Name";
            this.fullNameDataGridViewTextBoxColumn.MinimumWidth = 10;
            this.fullNameDataGridViewTextBoxColumn.Name = "fullNameDataGridViewTextBoxColumn";
            this.fullNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.fullNameDataGridViewTextBoxColumn.Width = 79;
            // 
            // pubDateDataGridViewTextBoxColumn
            // 
            this.pubDateDataGridViewTextBoxColumn.DataPropertyName = "PubDate";
            this.pubDateDataGridViewTextBoxColumn.HeaderText = "Publish Date";
            this.pubDateDataGridViewTextBoxColumn.Name = "pubDateDataGridViewTextBoxColumn";
            this.pubDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.pubDateDataGridViewTextBoxColumn.Width = 92;
            // 
            // statusDataGridViewTextBoxColumn1
            // 
            this.statusDataGridViewTextBoxColumn1.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn1.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn1.Name = "statusDataGridViewTextBoxColumn1";
            this.statusDataGridViewTextBoxColumn1.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn1.Width = 62;
            // 
            // doSaveDataGridViewCheckBoxColumn
            // 
            this.doSaveDataGridViewCheckBoxColumn.DataPropertyName = "DoSave";
            this.doSaveDataGridViewCheckBoxColumn.HeaderText = "Keep?";
            this.doSaveDataGridViewCheckBoxColumn.MinimumWidth = 60;
            this.doSaveDataGridViewCheckBoxColumn.Name = "doSaveDataGridViewCheckBoxColumn";
            this.doSaveDataGridViewCheckBoxColumn.ToolTipText = "Check this if you want rcFeedMe to never delete the file.";
            this.doSaveDataGridViewCheckBoxColumn.Width = 60;
            // 
            // totalBytesDataGridViewTextBoxColumn
            // 
            this.totalBytesDataGridViewTextBoxColumn.DataPropertyName = "TotalBytes";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.totalBytesDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.totalBytesDataGridViewTextBoxColumn.HeaderText = "Total Bytes";
            this.totalBytesDataGridViewTextBoxColumn.Name = "totalBytesDataGridViewTextBoxColumn";
            this.totalBytesDataGridViewTextBoxColumn.ReadOnly = true;
            this.totalBytesDataGridViewTextBoxColumn.Width = 85;
            // 
            // Synced
            // 
            this.Synced.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Synced.DataPropertyName = "Synced";
            this.Synced.HeaderText = "Synced";
            this.Synced.MinimumWidth = 65;
            this.Synced.Name = "Synced";
            this.Synced.ToolTipText = "If checked it means the file has already been synced and will not be synced again" +
                ".";
            this.Synced.Width = 65;
            // 
            // mnuHistory
            // 
            this.mnuHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHistoryPurge,
            this.mnuHistoryRemoveRows,
            this.mnuHistoryKeep,
            this.mnuHistoryNoKeep,
            this.mnuHistoryCheckSync,
            this.mnuHistoryUncheckSync});
            this.mnuHistory.Name = "mnuHistory";
            this.mnuHistory.Size = new System.Drawing.Size(262, 136);
            // 
            // mnuHistoryPurge
            // 
            this.mnuHistoryPurge.Name = "mnuHistoryPurge";
            this.mnuHistoryPurge.Size = new System.Drawing.Size(261, 22);
            this.mnuHistoryPurge.Text = "Purge History";
            this.mnuHistoryPurge.Click += new System.EventHandler(this.mnuHistoryPurge_Click);
            // 
            // mnuHistoryRemoveRows
            // 
            this.mnuHistoryRemoveRows.Name = "mnuHistoryRemoveRows";
            this.mnuHistoryRemoveRows.Size = new System.Drawing.Size(261, 22);
            this.mnuHistoryRemoveRows.Text = "Remove Selected Rows";
            this.mnuHistoryRemoveRows.Click += new System.EventHandler(this.mnuHistoryRemoveRows_Click);
            // 
            // mnuHistoryKeep
            // 
            this.mnuHistoryKeep.Name = "mnuHistoryKeep";
            this.mnuHistoryKeep.Size = new System.Drawing.Size(261, 22);
            this.mnuHistoryKeep.Text = "Check Keep? on Selected Rows";
            this.mnuHistoryKeep.Click += new System.EventHandler(this.mnuHistoryKeep_Click);
            // 
            // mnuHistoryNoKeep
            // 
            this.mnuHistoryNoKeep.Name = "mnuHistoryNoKeep";
            this.mnuHistoryNoKeep.Size = new System.Drawing.Size(261, 22);
            this.mnuHistoryNoKeep.Text = "Uncheck Keep? on Selected Rows";
            this.mnuHistoryNoKeep.Click += new System.EventHandler(this.mnuHistoryNoKeep_Click);
            // 
            // mnuHistoryCheckSync
            // 
            this.mnuHistoryCheckSync.Name = "mnuHistoryCheckSync";
            this.mnuHistoryCheckSync.Size = new System.Drawing.Size(261, 22);
            this.mnuHistoryCheckSync.Text = "Check Synced? on Selected Rows";
            this.mnuHistoryCheckSync.Click += new System.EventHandler(this.mnuHistoryCheckSync_Click);
            // 
            // mnuHistoryUncheckSync
            // 
            this.mnuHistoryUncheckSync.Name = "mnuHistoryUncheckSync";
            this.mnuHistoryUncheckSync.Size = new System.Drawing.Size(261, 22);
            this.mnuHistoryUncheckSync.Text = "Uncheck Synced? on Selected Rows";
            this.mnuHistoryUncheckSync.Click += new System.EventHandler(this.mnuHistoryUncheckSync_Click);
            // 
            // dsHistory
            // 
            this.dsHistory.DataSetName = "NewDataSet";
            this.dsHistory.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable4});
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn30,
            this.dataColumn31,
            this.dataColumn32,
            this.dataColumn33,
            this.dataColumn34,
            this.dataColumn35,
            this.dataColumn36,
            this.dataColumn37,
            this.dataColumn38,
            this.dataColumn48,
            this.dataColumn54});
            this.dataTable4.TableName = "Table1";
            // 
            // dataColumn30
            // 
            this.dataColumn30.ColumnName = "Name";
            // 
            // dataColumn31
            // 
            this.dataColumn31.ColumnName = "Title";
            // 
            // dataColumn32
            // 
            this.dataColumn32.ColumnName = "Summary";
            // 
            // dataColumn33
            // 
            this.dataColumn33.ColumnName = "Link";
            // 
            // dataColumn34
            // 
            this.dataColumn34.ColumnName = "FullName";
            // 
            // dataColumn35
            // 
            this.dataColumn35.ColumnName = "PubDate";
            this.dataColumn35.DataType = typeof(System.DateTime);
            // 
            // dataColumn36
            // 
            this.dataColumn36.ColumnName = "Status";
            // 
            // dataColumn37
            // 
            this.dataColumn37.ColumnName = "DoSave";
            this.dataColumn37.DataType = typeof(bool);
            // 
            // dataColumn38
            // 
            this.dataColumn38.ColumnName = "TotalBytes";
            this.dataColumn38.DataType = typeof(long);
            // 
            // dataColumn48
            // 
            this.dataColumn48.ColumnName = "FileName";
            // 
            // dataColumn54
            // 
            this.dataColumn54.ColumnName = "Synced";
            this.dataColumn54.DataType = typeof(bool);
            // 
            // tpErrors
            // 
            this.tpErrors.Controls.Add(this.dgvErrors);
            this.tpErrors.Location = new System.Drawing.Point(4, 22);
            this.tpErrors.Name = "tpErrors";
            this.tpErrors.Padding = new System.Windows.Forms.Padding(3);
            this.tpErrors.Size = new System.Drawing.Size(939, 574);
            this.tpErrors.TabIndex = 6;
            this.tpErrors.Text = "Recent Errors";
            this.tpErrors.ToolTipText = "Errors from the most recent queue that was processed.";
            this.tpErrors.UseVisualStyleBackColor = true;
            // 
            // dgvErrors
            // 
            this.dgvErrors.AllowUserToAddRows = false;
            this.dgvErrors.AllowUserToDeleteRows = false;
            this.dgvErrors.AllowUserToOrderColumns = true;
            this.dgvErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvErrors.AutoGenerateColumns = false;
            this.dgvErrors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvErrors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn4,
            this.titleDataGridViewTextBoxColumn3,
            this.summaryDataGridViewTextBoxColumn2,
            this.linkDataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn3,
            this.fullNameDataGridViewTextBoxColumn1,
            this.pubDateDataGridViewTextBoxColumn1,
            this.statusDataGridViewTextBoxColumn2});
            this.dgvErrors.DataMember = "Table1";
            this.dgvErrors.DataSource = this.dsErrors;
            this.dgvErrors.Location = new System.Drawing.Point(3, 3);
            this.dgvErrors.Name = "dgvErrors";
            this.dgvErrors.ReadOnly = true;
            this.dgvErrors.RowTemplate.Height = 24;
            this.dgvErrors.Size = new System.Drawing.Size(933, 560);
            this.dgvErrors.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn4
            // 
            this.nameDataGridViewTextBoxColumn4.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn4.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn4.Name = "nameDataGridViewTextBoxColumn4";
            this.nameDataGridViewTextBoxColumn4.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn4.Width = 60;
            // 
            // titleDataGridViewTextBoxColumn3
            // 
            this.titleDataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.titleDataGridViewTextBoxColumn3.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn3.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn3.MinimumWidth = 10;
            this.titleDataGridViewTextBoxColumn3.Name = "titleDataGridViewTextBoxColumn3";
            this.titleDataGridViewTextBoxColumn3.ReadOnly = true;
            this.titleDataGridViewTextBoxColumn3.Width = 150;
            // 
            // summaryDataGridViewTextBoxColumn2
            // 
            this.summaryDataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.summaryDataGridViewTextBoxColumn2.DataPropertyName = "Summary";
            this.summaryDataGridViewTextBoxColumn2.HeaderText = "Summary";
            this.summaryDataGridViewTextBoxColumn2.MinimumWidth = 10;
            this.summaryDataGridViewTextBoxColumn2.Name = "summaryDataGridViewTextBoxColumn2";
            this.summaryDataGridViewTextBoxColumn2.ReadOnly = true;
            this.summaryDataGridViewTextBoxColumn2.Width = 75;
            // 
            // linkDataGridViewTextBoxColumn3
            // 
            this.linkDataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.linkDataGridViewTextBoxColumn3.DataPropertyName = "Link";
            this.linkDataGridViewTextBoxColumn3.HeaderText = "Link";
            this.linkDataGridViewTextBoxColumn3.MinimumWidth = 10;
            this.linkDataGridViewTextBoxColumn3.Name = "linkDataGridViewTextBoxColumn3";
            this.linkDataGridViewTextBoxColumn3.ReadOnly = true;
            this.linkDataGridViewTextBoxColumn3.Width = 52;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "FileName";
            this.dataGridViewTextBoxColumn3.HeaderText = "FileName";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 76;
            // 
            // fullNameDataGridViewTextBoxColumn1
            // 
            this.fullNameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.fullNameDataGridViewTextBoxColumn1.DataPropertyName = "FullName";
            this.fullNameDataGridViewTextBoxColumn1.HeaderText = "Full Name";
            this.fullNameDataGridViewTextBoxColumn1.MinimumWidth = 10;
            this.fullNameDataGridViewTextBoxColumn1.Name = "fullNameDataGridViewTextBoxColumn1";
            this.fullNameDataGridViewTextBoxColumn1.ReadOnly = true;
            this.fullNameDataGridViewTextBoxColumn1.Width = 79;
            // 
            // pubDateDataGridViewTextBoxColumn1
            // 
            this.pubDateDataGridViewTextBoxColumn1.DataPropertyName = "PubDate";
            this.pubDateDataGridViewTextBoxColumn1.HeaderText = "Publish Date";
            this.pubDateDataGridViewTextBoxColumn1.Name = "pubDateDataGridViewTextBoxColumn1";
            this.pubDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.pubDateDataGridViewTextBoxColumn1.Width = 92;
            // 
            // statusDataGridViewTextBoxColumn2
            // 
            this.statusDataGridViewTextBoxColumn2.DataPropertyName = "Status";
            this.statusDataGridViewTextBoxColumn2.HeaderText = "Status";
            this.statusDataGridViewTextBoxColumn2.Name = "statusDataGridViewTextBoxColumn2";
            this.statusDataGridViewTextBoxColumn2.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn2.Width = 62;
            // 
            // dsErrors
            // 
            this.dsErrors.DataSetName = "NewDataSet";
            this.dsErrors.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable5});
            // 
            // dataTable5
            // 
            this.dataTable5.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn39,
            this.dataColumn40,
            this.dataColumn41,
            this.dataColumn42,
            this.dataColumn43,
            this.dataColumn44,
            this.dataColumn45,
            this.dataColumn46,
            this.dataColumn47,
            this.dataColumn49,
            this.dataColumn55});
            this.dataTable5.TableName = "Table1";
            // 
            // dataColumn39
            // 
            this.dataColumn39.ColumnName = "Name";
            // 
            // dataColumn40
            // 
            this.dataColumn40.ColumnName = "Title";
            // 
            // dataColumn41
            // 
            this.dataColumn41.ColumnName = "Summary";
            // 
            // dataColumn42
            // 
            this.dataColumn42.ColumnName = "Link";
            // 
            // dataColumn43
            // 
            this.dataColumn43.ColumnName = "FullName";
            // 
            // dataColumn44
            // 
            this.dataColumn44.ColumnName = "PubDate";
            this.dataColumn44.DataType = typeof(System.DateTime);
            // 
            // dataColumn45
            // 
            this.dataColumn45.ColumnName = "Status";
            // 
            // dataColumn46
            // 
            this.dataColumn46.ColumnName = "DoSave";
            this.dataColumn46.DataType = typeof(bool);
            // 
            // dataColumn47
            // 
            this.dataColumn47.ColumnName = "TotalBytes";
            this.dataColumn47.DataType = typeof(long);
            // 
            // dataColumn49
            // 
            this.dataColumn49.ColumnName = "FileName";
            // 
            // dataColumn55
            // 
            this.dataColumn55.ColumnName = "Synced";
            this.dataColumn55.DataType = typeof(bool);
            // 
            // tpGroups
            // 
            this.tpGroups.Controls.Add(this.dgvGroups);
            this.tpGroups.Location = new System.Drawing.Point(4, 22);
            this.tpGroups.Name = "tpGroups";
            this.tpGroups.Size = new System.Drawing.Size(939, 574);
            this.tpGroups.TabIndex = 2;
            this.tpGroups.Text = "Group Defaults";
            this.tpGroups.UseVisualStyleBackColor = true;
            // 
            // dgvGroups
            // 
            this.dgvGroups.AllowUserToOrderColumns = true;
            this.dgvGroups.AutoGenerateColumns = false;
            this.dgvGroups.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGroups.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn1,
            this.savePathDataGridViewTextBoxColumn,
            this.dataGridViewTextBoxColumn2,
            this.PlaylistPath,
            this.PlaylistCutoffDays,
            this.BareSyncPath,
            this.PreSyncExeGroup,
            this.PreSyncExeArgsGroup,
            this.NoSyncCopy});
            this.dgvGroups.DataMember = "dtGroups";
            this.dgvGroups.DataSource = this.dsGroups;
            this.dgvGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGroups.Location = new System.Drawing.Point(0, 0);
            this.dgvGroups.Name = "dgvGroups";
            this.dgvGroups.RowTemplate.Height = 24;
            this.dgvGroups.Size = new System.Drawing.Size(939, 574);
            this.dgvGroups.TabIndex = 0;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.Width = 60;
            // 
            // savePathDataGridViewTextBoxColumn
            // 
            this.savePathDataGridViewTextBoxColumn.DataPropertyName = "SavePath";
            this.savePathDataGridViewTextBoxColumn.HeaderText = "Save Path";
            this.savePathDataGridViewTextBoxColumn.Name = "savePathDataGridViewTextBoxColumn";
            this.savePathDataGridViewTextBoxColumn.Width = 76;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "SyncPath";
            this.dataGridViewTextBoxColumn2.HeaderText = "Sync Path";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 75;
            // 
            // PlaylistPath
            // 
            this.PlaylistPath.DataPropertyName = "PlaylistPath";
            this.PlaylistPath.HeaderText = "PlaylistPath";
            this.PlaylistPath.Name = "PlaylistPath";
            this.PlaylistPath.Width = 86;
            // 
            // PlaylistCutoffDays
            // 
            this.PlaylistCutoffDays.DataPropertyName = "PlaylistCutoffDays";
            this.PlaylistCutoffDays.HeaderText = "Playlist Cutoff Days";
            this.PlaylistCutoffDays.Name = "PlaylistCutoffDays";
            this.PlaylistCutoffDays.Width = 90;
            // 
            // BareSyncPath
            // 
            this.BareSyncPath.DataPropertyName = "BareSyncPath";
            this.BareSyncPath.HeaderText = "BareSyncPath";
            this.BareSyncPath.Name = "BareSyncPath";
            this.BareSyncPath.ToolTipText = "When Set, no Feed name is added to the Sync Path";
            this.BareSyncPath.Width = 81;
            // 
            // PreSyncExeGroup
            // 
            this.PreSyncExeGroup.DataPropertyName = "PreSyncExeGroup";
            this.PreSyncExeGroup.HeaderText = "PreSyncExe";
            this.PreSyncExeGroup.Name = "PreSyncExeGroup";
            this.PreSyncExeGroup.ToolTipText = "Exe or Script to run instead of doing a simple sync/copy (can be used to split mp" +
                "3 files)";
            this.PreSyncExeGroup.Width = 90;
            // 
            // PreSyncExeArgsGroup
            // 
            this.PreSyncExeArgsGroup.DataPropertyName = "PreSyncExeArgsGroup";
            this.PreSyncExeArgsGroup.HeaderText = "PreSyncExeArgs";
            this.PreSyncExeArgsGroup.Name = "PreSyncExeArgsGroup";
            this.PreSyncExeArgsGroup.ToolTipText = "Args to pass to Sync Script";
            this.PreSyncExeArgsGroup.Width = 111;
            // 
            // NoSyncCopy
            // 
            this.NoSyncCopy.DataPropertyName = "SuppressSyncCopy";
            this.NoSyncCopy.HeaderText = "NoSyncCopy";
            this.NoSyncCopy.Name = "NoSyncCopy";
            this.NoSyncCopy.ToolTipText = "When using a syn script, the default copy may be turned off using this flag";
            this.NoSyncCopy.Width = 75;
            // 
            // dsGroups
            // 
            this.dsGroups.DataSetName = "dsGroups";
            this.dsGroups.Tables.AddRange(new System.Data.DataTable[] {
            this.dtGroups});
            // 
            // dtGroups
            // 
            this.dtGroups.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn53,
            this.dataColumn60,
            this.dataColumn62,
            this.dataColumn70,
            this.dataColumn71,
            this.dataColumn72,
            this.dataColumn73});
            this.dtGroups.TableName = "dtGroups";
            // 
            // dataColumn8
            // 
            this.dataColumn8.AllowDBNull = false;
            this.dataColumn8.ColumnName = "Name";
            // 
            // dataColumn9
            // 
            this.dataColumn9.AllowDBNull = false;
            this.dataColumn9.ColumnName = "SavePath";
            // 
            // dataColumn53
            // 
            this.dataColumn53.ColumnName = "SyncPath";
            // 
            // dataColumn60
            // 
            this.dataColumn60.ColumnName = "PlaylistPath";
            // 
            // dataColumn62
            // 
            this.dataColumn62.ColumnName = "PlaylistCutoffDays";
            this.dataColumn62.DataType = typeof(int);
            // 
            // dataColumn70
            // 
            this.dataColumn70.ColumnName = "BareSyncPath";
            this.dataColumn70.DataType = typeof(bool);
            // 
            // dataColumn71
            // 
            this.dataColumn71.ColumnName = "PreSyncExeGroup";
            // 
            // dataColumn72
            // 
            this.dataColumn72.ColumnName = "PreSyncExeArgsGroup";
            // 
            // dataColumn73
            // 
            this.dataColumn73.ColumnName = "SuppressSyncCopy";
            this.dataColumn73.DataType = typeof(bool);
            // 
            // tpSettings
            // 
            this.tpSettings.Controls.Add(this.chkDupErrors);
            this.tpSettings.Controls.Add(this.lblSupressDupError);
            this.tpSettings.Controls.Add(this.tableLayoutPanel1);
            this.tpSettings.Location = new System.Drawing.Point(4, 22);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Size = new System.Drawing.Size(939, 574);
            this.tpSettings.TabIndex = 3;
            this.tpSettings.Text = "Settings";
            this.tpSettings.ToolTipText = "Global preferences.";
            this.tpSettings.UseVisualStyleBackColor = true;
            // 
            // chkDupErrors
            // 
            this.chkDupErrors.AutoSize = true;
            this.chkDupErrors.Location = new System.Drawing.Point(198, 419);
            this.chkDupErrors.Name = "chkDupErrors";
            this.chkDupErrors.Size = new System.Drawing.Size(15, 14);
            this.chkDupErrors.TabIndex = 2;
            this.chkDupErrors.UseVisualStyleBackColor = true;
            // 
            // lblSupressDupError
            // 
            this.lblSupressDupError.AutoSize = true;
            this.lblSupressDupError.Location = new System.Drawing.Point(6, 419);
            this.lblSupressDupError.Name = "lblSupressDupError";
            this.lblSupressDupError.Size = new System.Drawing.Size(106, 13);
            this.lblSupressDupError.TabIndex = 1;
            this.lblSupressDupError.Text = "Log Duplicate Errors:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.lblFreq, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFreq, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDefPath, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDefKeepCount, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtDefPath, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDefKeepCount, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblThreadScan, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblThreadDL, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtThreadScan, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtThreadDL, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblScanStall, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblDLStall, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblDLRetry, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.txtScanStall, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtDLStall, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtDLRetry, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.lblScanRetry, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.txtScanRetry, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblScanTimeout, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblDLTimeout, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtScanTimeout, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.txtDLTimeout, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.lblSyncPath, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtSyncPath, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTimeZones, 0, 18);
            this.tableLayoutPanel1.Controls.Add(this.lblDateFormats, 0, 19);
            this.tableLayoutPanel1.Controls.Add(this.txtTimezones, 1, 18);
            this.tableLayoutPanel1.Controls.Add(this.txtDateFormats, 1, 19);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 20;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(673, 404);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblFreq
            // 
            this.lblFreq.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFreq.AutoSize = true;
            this.lblFreq.Location = new System.Drawing.Point(3, 0);
            this.lblFreq.Name = "lblFreq";
            this.lblFreq.Size = new System.Drawing.Size(186, 26);
            this.lblFreq.TabIndex = 0;
            this.lblFreq.Text = "Frequency to process feeds (minutes):";
            this.lblFreq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFreq
            // 
            this.txtFreq.BackColor = System.Drawing.SystemColors.Window;
            this.txtFreq.Location = new System.Drawing.Point(195, 3);
            this.txtFreq.Name = "txtFreq";
            this.txtFreq.Size = new System.Drawing.Size(475, 20);
            this.txtFreq.TabIndex = 1;
            this.txtFreq.Text = "360";
            // 
            // lblDefPath
            // 
            this.lblDefPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDefPath.AutoSize = true;
            this.lblDefPath.Location = new System.Drawing.Point(3, 26);
            this.lblDefPath.Name = "lblDefPath";
            this.lblDefPath.Size = new System.Drawing.Size(97, 26);
            this.lblDefPath.TabIndex = 2;
            this.lblDefPath.Text = "Default Save Path:";
            this.lblDefPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDefKeepCount
            // 
            this.lblDefKeepCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDefKeepCount.AutoSize = true;
            this.lblDefKeepCount.Location = new System.Drawing.Point(3, 78);
            this.lblDefKeepCount.Name = "lblDefKeepCount";
            this.lblDefKeepCount.Size = new System.Drawing.Size(66, 26);
            this.lblDefKeepCount.TabIndex = 3;
            this.lblDefKeepCount.Text = "Keep Count:";
            this.lblDefKeepCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDefPath
            // 
            this.txtDefPath.Location = new System.Drawing.Point(195, 29);
            this.txtDefPath.Name = "txtDefPath";
            this.txtDefPath.Size = new System.Drawing.Size(475, 20);
            this.txtDefPath.TabIndex = 4;
            this.txtDefPath.Text = "c:\\podcasts";
            // 
            // txtDefKeepCount
            // 
            this.txtDefKeepCount.Location = new System.Drawing.Point(195, 81);
            this.txtDefKeepCount.Name = "txtDefKeepCount";
            this.txtDefKeepCount.Size = new System.Drawing.Size(475, 20);
            this.txtDefKeepCount.TabIndex = 5;
            this.txtDefKeepCount.Text = "10";
            // 
            // lblThreadScan
            // 
            this.lblThreadScan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblThreadScan.AutoSize = true;
            this.lblThreadScan.Location = new System.Drawing.Point(3, 124);
            this.lblThreadScan.Name = "lblThreadScan";
            this.lblThreadScan.Size = new System.Drawing.Size(115, 26);
            this.lblThreadScan.TabIndex = 6;
            this.lblThreadScan.Text = "Threads used to Scan:";
            this.lblThreadScan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblThreadDL
            // 
            this.lblThreadDL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblThreadDL.AutoSize = true;
            this.lblThreadDL.Location = new System.Drawing.Point(3, 150);
            this.lblThreadDL.Name = "lblThreadDL";
            this.lblThreadDL.Size = new System.Drawing.Size(136, 26);
            this.lblThreadDL.TabIndex = 7;
            this.lblThreadDL.Text = "Threads used to download:";
            this.lblThreadDL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtThreadScan
            // 
            this.txtThreadScan.Location = new System.Drawing.Point(195, 127);
            this.txtThreadScan.Name = "txtThreadScan";
            this.txtThreadScan.Size = new System.Drawing.Size(475, 20);
            this.txtThreadScan.TabIndex = 8;
            this.txtThreadScan.Text = "4";
            // 
            // txtThreadDL
            // 
            this.txtThreadDL.Location = new System.Drawing.Point(195, 153);
            this.txtThreadDL.Name = "txtThreadDL";
            this.txtThreadDL.Size = new System.Drawing.Size(475, 20);
            this.txtThreadDL.TabIndex = 9;
            this.txtThreadDL.Text = "2";
            // 
            // lblScanStall
            // 
            this.lblScanStall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblScanStall.AutoSize = true;
            this.lblScanStall.Location = new System.Drawing.Point(3, 176);
            this.lblScanStall.Name = "lblScanStall";
            this.lblScanStall.Size = new System.Drawing.Size(107, 26);
            this.lblScanStall.TabIndex = 10;
            this.lblScanStall.Text = "Scan Stall (seconds):";
            this.lblScanStall.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDLStall
            // 
            this.lblDLStall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDLStall.AutoSize = true;
            this.lblDLStall.Location = new System.Drawing.Point(3, 202);
            this.lblDLStall.Name = "lblDLStall";
            this.lblDLStall.Size = new System.Drawing.Size(130, 26);
            this.lblDLStall.TabIndex = 11;
            this.lblDLStall.Text = "Download Stall (seconds):";
            this.lblDLStall.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDLRetry
            // 
            this.lblDLRetry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDLRetry.AutoSize = true;
            this.lblDLRetry.Location = new System.Drawing.Point(3, 306);
            this.lblDLRetry.Name = "lblDLRetry";
            this.lblDLRetry.Size = new System.Drawing.Size(135, 26);
            this.lblDLRetry.TabIndex = 12;
            this.lblDLRetry.Text = "Download Retry (seconds):";
            this.lblDLRetry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtScanStall
            // 
            this.txtScanStall.Location = new System.Drawing.Point(195, 179);
            this.txtScanStall.Name = "txtScanStall";
            this.txtScanStall.Size = new System.Drawing.Size(475, 20);
            this.txtScanStall.TabIndex = 13;
            this.txtScanStall.Text = "30";
            // 
            // txtDLStall
            // 
            this.txtDLStall.Location = new System.Drawing.Point(195, 205);
            this.txtDLStall.Name = "txtDLStall";
            this.txtDLStall.Size = new System.Drawing.Size(475, 20);
            this.txtDLStall.TabIndex = 14;
            this.txtDLStall.Text = "30";
            // 
            // txtDLRetry
            // 
            this.txtDLRetry.Location = new System.Drawing.Point(195, 309);
            this.txtDLRetry.Name = "txtDLRetry";
            this.txtDLRetry.Size = new System.Drawing.Size(475, 20);
            this.txtDLRetry.TabIndex = 15;
            this.txtDLRetry.Text = "3";
            // 
            // lblScanRetry
            // 
            this.lblScanRetry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblScanRetry.AutoSize = true;
            this.lblScanRetry.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblScanRetry.Location = new System.Drawing.Point(3, 280);
            this.lblScanRetry.Name = "lblScanRetry";
            this.lblScanRetry.Size = new System.Drawing.Size(112, 26);
            this.lblScanRetry.TabIndex = 18;
            this.lblScanRetry.Text = "Scan Retry (seconds):";
            this.lblScanRetry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtScanRetry
            // 
            this.txtScanRetry.Location = new System.Drawing.Point(195, 283);
            this.txtScanRetry.Name = "txtScanRetry";
            this.txtScanRetry.Size = new System.Drawing.Size(475, 20);
            this.txtScanRetry.TabIndex = 20;
            this.txtScanRetry.Text = "3";
            // 
            // lblScanTimeout
            // 
            this.lblScanTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblScanTimeout.AutoSize = true;
            this.lblScanTimeout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblScanTimeout.Location = new System.Drawing.Point(3, 228);
            this.lblScanTimeout.Name = "lblScanTimeout";
            this.lblScanTimeout.Size = new System.Drawing.Size(125, 26);
            this.lblScanTimeout.TabIndex = 19;
            this.lblScanTimeout.Text = "Scan Timeout (seconds):";
            this.lblScanTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDLTimeout
            // 
            this.lblDLTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDLTimeout.AutoSize = true;
            this.lblDLTimeout.Location = new System.Drawing.Point(3, 254);
            this.lblDLTimeout.Name = "lblDLTimeout";
            this.lblDLTimeout.Size = new System.Drawing.Size(148, 26);
            this.lblDLTimeout.TabIndex = 17;
            this.lblDLTimeout.Text = "Download Timeout (seconds):";
            this.lblDLTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtScanTimeout
            // 
            this.txtScanTimeout.Location = new System.Drawing.Point(195, 231);
            this.txtScanTimeout.Name = "txtScanTimeout";
            this.txtScanTimeout.Size = new System.Drawing.Size(475, 20);
            this.txtScanTimeout.TabIndex = 21;
            this.txtScanTimeout.Text = "300";
            // 
            // txtDLTimeout
            // 
            this.txtDLTimeout.Location = new System.Drawing.Point(195, 257);
            this.txtDLTimeout.Name = "txtDLTimeout";
            this.txtDLTimeout.Size = new System.Drawing.Size(475, 20);
            this.txtDLTimeout.TabIndex = 16;
            this.txtDLTimeout.Text = "14400";
            // 
            // lblSyncPath
            // 
            this.lblSyncPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSyncPath.AutoSize = true;
            this.lblSyncPath.Location = new System.Drawing.Point(3, 52);
            this.lblSyncPath.Name = "lblSyncPath";
            this.lblSyncPath.Size = new System.Drawing.Size(186, 26);
            this.lblSyncPath.TabIndex = 22;
            this.lblSyncPath.Text = "Default Sync Path:";
            this.lblSyncPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSyncPath
            // 
            this.txtSyncPath.Location = new System.Drawing.Point(195, 55);
            this.txtSyncPath.Name = "txtSyncPath";
            this.txtSyncPath.Size = new System.Drawing.Size(475, 20);
            this.txtSyncPath.TabIndex = 23;
            // 
            // lblTimeZones
            // 
            this.lblTimeZones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTimeZones.AutoSize = true;
            this.lblTimeZones.Location = new System.Drawing.Point(3, 352);
            this.lblTimeZones.Name = "lblTimeZones";
            this.lblTimeZones.Size = new System.Drawing.Size(186, 26);
            this.lblTimeZones.TabIndex = 24;
            this.lblTimeZones.Text = "Time Zones:";
            this.lblTimeZones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateFormats
            // 
            this.lblDateFormats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDateFormats.AutoSize = true;
            this.lblDateFormats.Location = new System.Drawing.Point(3, 378);
            this.lblDateFormats.Name = "lblDateFormats";
            this.lblDateFormats.Size = new System.Drawing.Size(186, 26);
            this.lblDateFormats.TabIndex = 25;
            this.lblDateFormats.Text = "Date Formats:";
            this.lblDateFormats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTimezones
            // 
            this.txtTimezones.Location = new System.Drawing.Point(195, 355);
            this.txtTimezones.Name = "txtTimezones";
            this.txtTimezones.Size = new System.Drawing.Size(475, 20);
            this.txtTimezones.TabIndex = 26;
            // 
            // txtDateFormats
            // 
            this.txtDateFormats.Location = new System.Drawing.Point(195, 381);
            this.txtDateFormats.Name = "txtDateFormats";
            this.txtDateFormats.Size = new System.Drawing.Size(475, 20);
            this.txtDateFormats.TabIndex = 27;
            // 
            // tpLog
            // 
            this.tpLog.Controls.Add(this.txtLog);
            this.tpLog.Location = new System.Drawing.Point(4, 22);
            this.tpLog.Name = "tpLog";
            this.tpLog.Padding = new System.Windows.Forms.Padding(3);
            this.tpLog.Size = new System.Drawing.Size(939, 574);
            this.tpLog.TabIndex = 1;
            this.tpLog.Text = "Log";
            this.tpLog.ToolTipText = "Detailed self triming log.";
            this.tpLog.UseVisualStyleBackColor = true;
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.BackColor = System.Drawing.Color.Gainsboro;
            this.txtLog.ContextMenuStrip = this.mnuLog;
            this.txtLog.Location = new System.Drawing.Point(0, 0);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(939, 570);
            this.txtLog.TabIndex = 0;
            // 
            // mnuLog
            // 
            this.mnuLog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuLogRefresh});
            this.mnuLog.Name = "mnuLog";
            this.mnuLog.Size = new System.Drawing.Size(114, 26);
            // 
            // mnuLogRefresh
            // 
            this.mnuLogRefresh.Name = "mnuLogRefresh";
            this.mnuLogRefresh.Size = new System.Drawing.Size(113, 22);
            this.mnuLogRefresh.Text = "Refresh";
            this.mnuLogRefresh.Click += new System.EventHandler(this.mnuLogRefresh_Click);
            // 
            // tpErrorLog
            // 
            this.tpErrorLog.Controls.Add(this.txtErrorLog);
            this.tpErrorLog.Location = new System.Drawing.Point(4, 22);
            this.tpErrorLog.Margin = new System.Windows.Forms.Padding(2);
            this.tpErrorLog.Name = "tpErrorLog";
            this.tpErrorLog.Padding = new System.Windows.Forms.Padding(2);
            this.tpErrorLog.Size = new System.Drawing.Size(939, 574);
            this.tpErrorLog.TabIndex = 7;
            this.tpErrorLog.Text = "Error Log";
            this.tpErrorLog.UseVisualStyleBackColor = true;
            // 
            // txtErrorLog
            // 
            this.txtErrorLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtErrorLog.BackColor = System.Drawing.Color.Gainsboro;
            this.txtErrorLog.ContextMenuStrip = this.mnuErrorLog;
            this.txtErrorLog.Location = new System.Drawing.Point(2, 3);
            this.txtErrorLog.Multiline = true;
            this.txtErrorLog.Name = "txtErrorLog";
            this.txtErrorLog.ReadOnly = true;
            this.txtErrorLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtErrorLog.Size = new System.Drawing.Size(939, 570);
            this.txtErrorLog.TabIndex = 1;
            // 
            // mnuErrorLog
            // 
            this.mnuErrorLog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuErrorLogRefresh});
            this.mnuErrorLog.Name = "mnuErrorLog";
            this.mnuErrorLog.Size = new System.Drawing.Size(114, 26);
            // 
            // mnuErrorLogRefresh
            // 
            this.mnuErrorLogRefresh.Name = "mnuErrorLogRefresh";
            this.mnuErrorLogRefresh.Size = new System.Drawing.Size(113, 22);
            this.mnuErrorLogRefresh.Text = "Refresh";
            this.mnuErrorLogRefresh.Click += new System.EventHandler(this.mnuErrorLogRefresh_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "OPML files (*.opml)|*.opml|XML files (*.xml)|*.xml|All files (*.*)|*.*";
            this.openFileDialog1.Title = "Select OPML file to import";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "rcFeedMe.opml";
            this.saveFileDialog1.Filter = "OPML files (*.opml)|*.opml|XML files (*.xml)|*.xml|All files (*.*)|*.*";
            this.saveFileDialog1.Title = "Select OPML file to export to";
            // 
            // timerDelayStart
            // 
            this.timerDelayStart.Enabled = true;
            this.timerDelayStart.Tick += new System.EventHandler(this.timerDelayStart_Tick);
            // 
            // dataGridViewProgressColumn1
            // 
            this.dataGridViewProgressColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewProgressColumn1.DataPropertyName = "Progress";
            this.dataGridViewProgressColumn1.HeaderText = "Progress";
            this.dataGridViewProgressColumn1.MinimumWidth = 100;
            this.dataGridViewProgressColumn1.Name = "dataGridViewProgressColumn1";
            // 
            // dataGridViewProgressColumn2
            // 
            this.dataGridViewProgressColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewProgressColumn2.DataPropertyName = "Progress";
            this.dataGridViewProgressColumn2.HeaderText = "Progress";
            this.dataGridViewProgressColumn2.MinimumWidth = 100;
            this.dataGridViewProgressColumn2.Name = "dataGridViewProgressColumn2";
            this.dataGridViewProgressColumn2.ReadOnly = true;
            // 
            // timerDelayStop
            // 
            this.timerDelayStop.Interval = 1000;
            this.timerDelayStop.Tick += new System.EventHandler(this.timerDelayStop_Tick);
            // 
            // clearAllSyncFlagsToolStripMenuItem
            // 
            this.clearAllSyncFlagsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clearAllSyncFlagsToolStripMenuItem.Image")));
            this.clearAllSyncFlagsToolStripMenuItem.Name = "clearAllSyncFlagsToolStripMenuItem";
            this.clearAllSyncFlagsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.clearAllSyncFlagsToolStripMenuItem.Text = "Clear All Sync Flags";
            this.clearAllSyncFlagsToolStripMenuItem.Click += new System.EventHandler(this.clearAllSyncFlagsToolStripMenuItem_Click);
            // 
            // frmRcFeedMe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 677);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.mnuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnuStrip;
            this.Name = "frmRcFeedMe";
            this.Text = "rcFeedMe";
            this.mnuStrip.ResumeLayout(false);
            this.mnuStrip.PerformLayout();
            this.mnuTray.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tpFeeds.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGroupDD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.mnuFeeds.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsFeeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFeeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFeed)).EndInit();
            this.mnuFeed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsFeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            this.tpQueue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQueue)).EndInit();
            this.mnuQueue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsQueue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            this.tpHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).EndInit();
            this.mnuHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            this.tpErrors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).EndInit();
            this.tpGroups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGroups)).EndInit();
            this.tpSettings.ResumeLayout(false);
            this.tpSettings.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tpLog.ResumeLayout(false);
            this.tpLog.PerformLayout();
            this.mnuLog.ResumeLayout(false);
            this.tpErrorLog.ResumeLayout(false);
            this.tpErrorLog.PerformLayout();
            this.mnuErrorLog.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }      

        #endregion

        private System.Windows.Forms.MenuStrip mnuStrip;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Timer timerSched;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tpFeeds;
        private System.Windows.Forms.TabPage tpLog;
        private System.Windows.Forms.ToolStripButton tsbStartSched;
        private System.Windows.Forms.ToolStripButton tsbStopSched;
        private System.Windows.Forms.ToolStripMenuItem mnuAbout;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private gfoidl.Windows.Forms.gfDataGridView dgvFeeds;
        private System.Windows.Forms.TabPage tpGroups;
        private gfoidl.Windows.Forms.gfDataGridView dgvGroups;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblFreq;
        private System.Windows.Forms.TextBox txtFreq;
        private System.Windows.Forms.Label lblDefPath;
        private System.Windows.Forms.Label lblDefKeepCount;
        private System.Windows.Forms.TextBox txtDefPath;
        private System.Windows.Forms.TextBox txtDefKeepCount;
        private System.Windows.Forms.Label lblThreadScan;
        private System.Windows.Forms.Label lblThreadDL;
        private System.Windows.Forms.TextBox txtThreadScan;
        private System.Windows.Forms.TextBox txtThreadDL;
        private System.Windows.Forms.ToolStripMenuItem mnuImportOpml;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Data.DataSet dsFeeds;
        private System.Data.DataTable dtFeeds;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataSet dsGroups;
        private System.Data.DataTable dtGroups;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataSet dsGroupDD;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Windows.Forms.TextBox txtLog;
        private gfoidl.Windows.Forms.gfDataGridView dgvFeed;
        private System.Data.DataSet dsFeed;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Windows.Forms.ToolStripButton tsbRefreshAll;
        private System.Windows.Forms.Label lblScanStall;
        private System.Windows.Forms.Label lblDLStall;
        private System.Windows.Forms.Label lblDLRetry;
        private System.Windows.Forms.TextBox txtScanStall;
        private System.Windows.Forms.TextBox txtDLStall;
        private System.Windows.Forms.TextBox txtDLRetry;
        private System.Windows.Forms.TextBox txtDLTimeout;
        private System.Windows.Forms.Label lblDLTimeout;
        private System.Windows.Forms.TabPage tpQueue;
        private System.Windows.Forms.ToolStripProgressBar progBar;
        private gfoidl.Windows.Forms.gfDataGridView dgvQueue;
        private System.Windows.Forms.TabPage tpHistory;
        private gfoidl.Windows.Forms.gfDataGridView dgvHistory;
        private System.Data.DataSet dsQueue;
        private System.Data.DataTable dataTable3;
        private System.Data.DataSet dsHistory;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataColumn dataColumn25;
        private System.Data.DataColumn dataColumn26;
        private System.Data.DataColumn dataColumn27;
        private System.Data.DataColumn dataColumn28;
        private System.Data.DataColumn dataColumn29;
        private System.Windows.Forms.Label lblScanRetry;
        private System.Windows.Forms.Label lblScanTimeout;
        private System.Windows.Forms.TextBox txtScanRetry;
        private System.Windows.Forms.TextBox txtScanTimeout;
        private System.Data.DataTable dataTable4;
        private System.Data.DataColumn dataColumn30;
        private System.Data.DataColumn dataColumn31;
        private System.Data.DataColumn dataColumn32;
        private System.Data.DataColumn dataColumn33;
        private System.Data.DataColumn dataColumn34;
        private System.Data.DataColumn dataColumn35;
        private System.Data.DataColumn dataColumn36;
        private System.Data.DataColumn dataColumn37;
        private System.Data.DataColumn dataColumn38;
        private System.Windows.Forms.ContextMenuStrip mnuHistory;
        private System.Windows.Forms.ToolStripMenuItem mnuHistoryPurge;
        private System.Windows.Forms.TabPage tpErrors;
        private gfoidl.Windows.Forms.gfDataGridView dgvErrors;
        private System.Data.DataSet dsErrors;
        private System.Data.DataTable dataTable5;
        private System.Data.DataColumn dataColumn39;
        private System.Data.DataColumn dataColumn40;
        private System.Data.DataColumn dataColumn41;
        private System.Data.DataColumn dataColumn42;
        private System.Data.DataColumn dataColumn43;
        private System.Data.DataColumn dataColumn44;
        private System.Data.DataColumn dataColumn45;
        private System.Data.DataColumn dataColumn46;
        private System.Data.DataColumn dataColumn47;
        private System.Windows.Forms.ContextMenuStrip mnuQueue;
        private System.Windows.Forms.ToolStripMenuItem mnuQueueBuild;
        private System.Windows.Forms.ToolStripMenuItem mnuQueueProcess;
        private System.Windows.Forms.ToolStripMenuItem mnuQueueStopAll;
        private System.Data.DataColumn dataColumn48;
        private System.Data.DataColumn dataColumn49;
        private System.Data.DataColumn dataColumn50;
        private System.Windows.Forms.ContextMenuStrip mnuFeed;
        private System.Windows.Forms.ToolStripMenuItem mnuFeedAddSelected;
        private System.Windows.Forms.ToolStripMenuItem mnuDebug;
        private System.Windows.Forms.ToolStripMenuItem mnuSaveSettings;
        private System.Windows.Forms.ToolStripMenuItem mnuLoadSettings;
        private System.Windows.Forms.ToolStripButton mnuRunBatch;
        private System.Windows.Forms.ContextMenuStrip mnuTray;
        private System.Windows.Forms.ToolStripMenuItem mnuTrayStart;
        private System.Windows.Forms.ToolStripMenuItem mnuTrayStop;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mnuTrayExit;
        private Sample.DataGridViewProgressColumn dataGridViewProgressColumn1;
        private System.Windows.Forms.ContextMenuStrip mnuFeeds;
        private System.Windows.Forms.ToolStripMenuItem mnuFeedsRemove;
        private System.Windows.Forms.ToolStripMenuItem mnuHistoryRemoveRows;
        private System.Windows.Forms.ToolStripMenuItem mnuQueueRemoveRows;
        private System.Windows.Forms.ToolStripMenuItem mnuQueueMoveToHistory;
        private System.Windows.Forms.ContextMenuStrip mnuLog;
        private System.Windows.Forms.ToolStripMenuItem mnuLogRefresh;
        private System.Windows.Forms.ToolStripStatusLabel tsStatus2;
        private System.Windows.Forms.ToolStripMenuItem mnuExportOpml;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Data.DataColumn dataColumn19;
        private System.Windows.Forms.ToolStripMenuItem mnuHistoryKeep;
        private System.Windows.Forms.ToolStripMenuItem mnuHistoryNoKeep;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn linkDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fileNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtPubDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Bytes;
        private Sample.DataGridViewProgressColumn Progress;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attempt;
        private System.Windows.Forms.Timer timerDelayStart;
        private System.Windows.Forms.ToolStripMenuItem mnuDumpXml;
        private System.Windows.Forms.ToolStripMenuItem mnuFeedsRefreshSelected;
        private System.Windows.Forms.ToolStripButton tsbSaveReloadSettings;
        private System.Data.DataColumn dataColumn51;
        private System.Data.DataColumn dataColumn52;
        private System.Data.DataColumn dataColumn53;
        private System.Data.DataColumn dataColumn54;
        private System.Windows.Forms.ToolStripMenuItem mnuHistoryCheckSync;
        private System.Windows.Forms.ToolStripMenuItem mnuHistoryUncheckSync;
        private System.Windows.Forms.Label lblSyncPath;
        private System.Windows.Forms.TextBox txtSyncPath;
        private System.Windows.Forms.ToolStripButton btnSync;
        private System.Data.DataColumn dataColumn55;
        private System.Windows.Forms.Label lblTimeZones;
        private System.Windows.Forms.Label lblDateFormats;
        private System.Windows.Forms.TextBox txtTimezones;
        private System.Windows.Forms.TextBox txtDateFormats;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbSearchHistory;
        private System.Windows.Forms.ToolStripTextBox txtSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuFeedsStopRefresh;
        private System.Data.DataColumn dataColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn summaryDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn linkDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pubDateDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn2;
        private System.Data.DataColumn dataColumn57;
        private System.Windows.Forms.ToolStripMenuItem mnuDebugSyncCount;
        private System.Windows.Forms.TabPage tpErrorLog;
        private System.Windows.Forms.TextBox txtErrorLog;
        private System.Windows.Forms.ContextMenuStrip mnuErrorLog;
        private System.Windows.Forms.ToolStripMenuItem mnuErrorLogRefresh;
        private System.Windows.Forms.ToolStripMenuItem mnuRestore;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn summaryDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn linkDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pubDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn doSaveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalBytesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Synced;
        private System.Data.DataColumn dataColumn58;
        private System.Data.DataColumn dataColumn59;
        private System.Data.DataColumn dataColumn61;
        private System.Data.DataColumn dataColumn60;
        private System.Data.DataColumn dataColumn62;
        private Sample.DataGridViewProgressColumn dataGridViewProgressColumn2;
        private System.Windows.Forms.ToolStripMenuItem mnuGeneratePlaylists;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Data.DataColumn dataColumn63;
        private System.Data.DataColumn dataColumn64;
        private System.Windows.Forms.Timer timerDelayStop;
        private System.Windows.Forms.CheckBox chkDupErrors;
        private System.Windows.Forms.Label lblSupressDupError;
        private System.Data.DataColumn dataColumn65;
        private System.Data.DataColumn dataColumn66;
        private System.Data.DataColumn dataColumn67;
        private System.Data.DataColumn dataColumn68;
        private System.Data.DataColumn dataColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn urlDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn keepCountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn includeExtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn includeTextDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn SavePath;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PrependDate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PrependTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn SyncPath;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DoSync;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DoCacheFix;
        private System.Windows.Forms.DataGridViewTextBoxColumn Username;
        private System.Windows.Forms.DataGridViewTextBoxColumn Password;
        private System.Windows.Forms.DataGridViewTextBoxColumn Priority;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DoDecode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PrependShortDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostDLExe;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostDLArgs;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreSyncExe;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreSyncArgs;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SuppressSyncCopy;
        private System.Windows.Forms.DataGridViewButtonColumn btnAddQueue;
        private System.Windows.Forms.DataGridViewCheckBoxColumn InQueue;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn summaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtPub;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn linkDataGridViewTextBoxColumn;
        private System.Data.DataColumn dataColumn70;
        private System.Data.DataColumn dataColumn71;
        private System.Data.DataColumn dataColumn72;
        private System.Data.DataColumn dataColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn savePathDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlaylistPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlaylistCutoffDays;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BareSyncPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreSyncExeGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreSyncExeArgsGroup;
        private System.Windows.Forms.DataGridViewCheckBoxColumn NoSyncCopy;
        private System.Windows.Forms.ToolStripMenuItem clearAllSyncFlagsToolStripMenuItem;
    }
}

